Dado(/^que acesse o menu de pagamento para pagar um boleto neon$/) do
  step 'que acesse o menu de depósitos'
  step 'realizar a geração de um boleto "normal"'
  step 'devo visualizar o boleto'
  page(Pagamento).acessar_pagamentos
  page(Pagamento).acessar_codigo_de_barras
end

Dado(/^que acesse o menu de pagamento$/) do
  step 'que acesse o aplicativo Neon'
  step 'realizar o login com um "cliente_pf"'
  page(Pagamento).acessar_pagamentos
  page(Pagamento).acessar_codigo_de_barras
end

Quando(/^realizar o pagamento de boleto$/) do
  page(Pagamento).digitar_codigo_barras(@num_boleto_neon)
  page(Comum).validar_texto('pagamentos', 'Digite o código de barras do seu boleto.')
  page(Comum).botao_proximo
  page(Comum).validar_texto('dados do pagamento', 'Hoje', 'Valor a pagar', 'Vence em', 'Data de pagamento')
  @data_venc = page(Comum).retornar_texto_do_elemento('payment_data_txt_date')
  @valor = page(Comum).retornar_texto_do_elemento('payment_data_edt_value_bill')
  puts "Data de vencimento: #{@data_venc} / Valor: #{@valor}"
  page(Comum).botao_proximo
  page(Comum).validar_texto('organizar', 'Descrição', 'Categoria')
  page(Comum).selecionar_categoria
  page(Pagamento).botao_finalizar
  page(Comum).digitar_senha_cartao(4)
end

Então(/^devo visualizar o comprovante do pagamento$/) do
  page(Comum).contem_texto('feito')
  @pago = page(Comum).retornar_texto_do_elemento('receipt_tv_payment_date_value')
  @usuario = page(Comum).retornar_texto_do_elemento('receipt_tv_name')
  @ag = page(Comum).retornar_texto_do_elemento('receipt_tv_agency')
  @conta = page(Comum).retornar_texto_do_elemento('receipt_tv_account')
  @descricao = page(Comum).retornar_texto_do_elemento('receipt_tv_description')
  @vencimento = page(Comum).retornar_texto_do_elemento('receipt_tv_due_date_value')
  @valor = page(Comum).retornar_texto_do_elemento('receipt_tv_payment_value')
  @nun_doc = page(Comum).retornar_texto_do_elemento('receipt_tv_document_number')
  puts "Data do pagamento: #{@pago} / Usuario: #{@usuario} / #{@ag} / #{@conta} / Descrição: #{@descricao} / Vencimento: #{@vencimento} / Valor pago: #{@valor} / Numero documento: #{@nun_doc}"
end

Quando(/^realizar o agendamento do pagamento de boleto outro banco$/) do
  page(Pagamento).digitar_codigo_barras($boleto_itau.linha_digitavel)
  page(Comum).botao_proximo
  page(Pagamento).acessar_data
  page(Comum).selecionar_data(2018, 12, 10)
  @data_venc = page(Comum).retornar_texto_do_elemento('payment_data_txt_date')
  @valor = page(Comum).retornar_texto_do_elemento('payment_data_edt_value_bill')
  puts "Data de vencimento: #{@data_venc} / Valor: #{@valor}"
  page(Comum).botao_proximo
  page(Comum).validar_texto('organizar', 'Descrição', 'Categoria')
  page(Comum).retornar_texto_do_elemento('payment_desc_cat_edt_desc')
  page(Comum).selecionar_categoria
  page(Pagamento).botao_finalizar
  page(Comum).digitar_senha_cartao(4)
end

Quando(/^realizar pagamento de boleto outro banco$/) do
  page(Pagamento).digitar_codigo_barras($boleto_itau2.linha_digitavel)
  page(Comum).botao_proximo
  @data_venc = page(Comum).retornar_texto_do_elemento('payment_data_txt_date')
  @valor = page(Comum).retornar_texto_do_elemento('payment_data_edt_value_bill')
  puts "Data de vencimento: #{@data_venc} / Valor: #{@valor}"
  page(Comum).validar_texto('dados do pagamento', 'Data de pagamento', 'Vence em')
  page(Comum).botao_proximo
  page(Comum).validar_texto('organizar', 'Descrição', 'Categoria')
  page(Comum).retornar_texto_do_elemento('payment_desc_cat_edt_desc')
  page(Comum).selecionar_categoria
  page(Pagamento).botao_finalizar
  page(Comum).digitar_senha_cartao(4)
end

Quando(/^realizar o agendamento do pagamento de boleto$/) do
  page(Pagamento).digitar_codigo_barras(@num_boleto_neon)
  page(Comum).botao_proximo
  page(Comum).validar_texto('dados do pagamento', 'Hoje', 'Valor a pagar', 'Vence em')
  @data_venc = page(Comum).retornar_texto_do_elemento('payment_data_txt_date')
  @valor = page(Comum).retornar_texto_do_elemento('payment_data_edt_value_bill')
  puts "Data de vencimento: #{@data_venc} / Valor: #{@valor}"
  page(Pagamento).acessar_data
  page(Comum).selecionar_data(2018, 4, 2)
  page(Comum).botao_proximo
  page(Comum).selecionar_categoria
  page(Pagamento).botao_finalizar
  page(Comum).digitar_senha_cartao(4)
end
