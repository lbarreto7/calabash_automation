Dado(/^que acesse a opção iniciar cadastro na tela inicial do App Neon$/) do
  if (ENV['ENV']) == 'DEV'
    puts 'AMBIENTE  - DEV'
    page(Login).on_boarding_login
    page(Cadastro).btn_abrir_conta
  else
    page(Comum).selecionar_ambiente(ENV['ENV'])
  end
end

Quando(/^informar todos os dados solicitados$/) do
  page(Cadastro).btn_config
  page(Cadastro).config_amb
  page(Cadastro).ck_termos_aceite
  page(Cadastro).btn_iniciar_cadastro
  @CPF = Faker::CPF.numeric
  page(Cadastro).informa_CPF(@CPF)
  page(Cadastro).txt_nome_completo
  page(Cadastro).data_nasc
  page(Cadastro).email_cadastro
  page(Cadastro).txt_nome_mae
  page(Cadastro).edt_cep
  page(Cadastro).edt_senha
  page(Cadastro).confirm_senha
  page(Cadastro).nm_celular
   @response = page(TokenCadastro).get_token_cadastro(@CPF)
   response = JSON.parse(@response.body)
   token = response['token']
  page(Cadastro).input_token(token)
  page(Cadastro).resp_questoes_selfie
  page(Cadastro).nm_renda
  page(Cadastro).btn_CNH
  page(Cadastro).btn_foto
  page(Cadastro).btn_confirm_image
  page(Cadastro).resp_questoes_doc
  page(Cadastro).ck_endereco
end

Então(/^devo aguardar a analise do meu cadastro para ser aprovado ou reprovado$/) do
  page(Cadastro).edt_assinatura
  default_device.shutdown_test_server
end
