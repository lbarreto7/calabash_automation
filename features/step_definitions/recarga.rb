Dado(/^acesse o menu de recarga$/) do
  page(Recarga).acessar_recarga
end

Quando(/^realizar uma recarga$/) do
  page(Comum).validar_texto('recargas')
  page(Recarga).escolher_celular
  page(Recarga).botao_add_numero
  page(Recarga).informar_nome
  page(Recarga).informar_numero
  page(Recarga).informar_ddd
  page(Comum).botao_entendi
  page(Recarga).nao_add_agenda
  page(Recarga).selecionar_operadora
  page(Comum).botao_entendi
  page(Recarga).selecionar_valor(1)
  page(Comum).botao_entendi
  page(Comum).validar_texto('confirmar', 'Automacao', 'Oi', '20', ',00')
  page(Recarga).repetir_recarga
  page(Comum).botao_entendi
  page(Comum).digitar_senha_cartao(4)
end

Então(/^devo visualizar o comprovante da recarga$/) do
  page(Comum).validar_texto('recarga feita', 'COMPARTILHAR')
  @comp = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_number')
  @data = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_date')
  @usuario = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_from_name')
  @num_cel = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_phone_value')
  @operadora = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_vendor_value')
  @valor = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_credit_value')
  @nsu = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_nsu_value')
  puts "Comprovante:#{@comp} / Data da Regarga:#{@data} / Usuário:#{@usuario} / Celular:#{@num_cel} / Operadora:#{@operadora} / Valor:#{@valor} / NSU:#{@nsu}"
end

Quando(/^realizar exclusão de uma recarga avulsa$/) do
  page(Recarga).selecionoar_numero_cadastrado('Automacao')
  @numero = page(Comum).retornar_texto_do_elemento('phone_recharge_confirm_txt_user_phone')
  page(Recarga).botao_excluir
  page(Recarga).confirmar_exclusao
end

Então(/^nao devo mais visualizar o numero na lista$/) do
  page(Comum).validar_texto('recargas', 'avulsas', 'recorrentes')
end

Quando(/^realizar uma recarga programda$/) do
  page(Recarga).escolher_celular
  page(Recarga).selecionoar_numero_cadastrado('Programada')
  page(Comum).botao_proximo
  page(Recarga).selecionar_operadora
  page(Comum).botao_entendi
  page(Recarga).selecionar_valor(1)
  page(Comum).botao_entendi
  page(Comum).botao_proximo
  page(Comum).contem_texto('........ R$')
  page(Recarga).recarga_recorrente
  page(Comum).digitar_senha_cartao(4)
end

Então(/^devo visualizar a recarga cadastrada$/) do
  page(Comum).validar_texto('recarga agendada', 'COMPARTILHAR')
  @data = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_date')
  @usuario = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_from_name')
  @num_cel = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_phone_value')
  @operadora = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_vendor_value')
  @valor = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_credit_value')
  puts "Data da Regarga:#{@data} / Usuário:#{@usuario} / Celular:#{@num_cel} / Operadora:#{@operadora} / Valor:#{@valor}"
end


Quando("selecionar recorrentes") do
  page(Comum).validar_texto('recorrentes')
  page(Recarga).selecionar_recorrentes
  page(Recarga).selecionar_telefone
  page(Comum).validar_texto('Valor')
  page(Recarga).selecionar_valor
  page(Comum).validar_texto('qual valor?')
  page(Recarga).alterar_valor
  page(Recarga).alterar_dia_cobranca
  page(Recarga).confirmar_edicao
  page(Comum).digitar_senha_cartao(4)
end

Então("devo visualizar a recarga recorrentes") do
  page(Comum).validar_texto('recarga agendada', 'COMPARTILHAR')
  @data = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_date')
  @usuario = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_from_name')
  @num_cel = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_phone_value')
  @operadora = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_vendor_value')
  @valor = page(Comum).retornar_texto_do_elemento('phone_recharge_receipt_lbl_credit_value')
  puts "Data da Regarga:#{@data} / Usuário:#{@usuario} / Celular:#{@num_cel} / Operadora:#{@operadora} / Valor:#{@valor} / RECARGA AGENDADA PARA:#{@AGENDADA} /Datahora:#{@datahora}"
end

Quando("realizar exclusão de uma recarga programada") do
  page(Recarga).acessar_recorrentes
  page(Recarga).remover_recorrentes
  page(Recarga).botao_excluir
  page(Recarga).confirmar_exclusao
end
