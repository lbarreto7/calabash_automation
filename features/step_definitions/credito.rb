Dado(/^que acesso o menu de crédito$/) do
  step 'que acesse o aplicativo Neon'
  step 'realizar o login com um "cliente_pf"'
  page(Credito).acessar
  page(Credito).swipe_esquerda(3)
  page(Credito).onboarding_botao
  page(Credito).botao_comecar
end
#Pagamento de fatura
Quando(/^realizo o pagamento da fatura com o valor total gerando o boleto$/) do
  page(Credito).botao_pagar_fatura
  page(Credito).botao_proximo
  page(Credito).botao_pagar_parcela
  page(Credito).botao_gerar_boleto
  page(Credito).botao_gerar_boleto_mesmo_assim
  page(Credito).botao_copiar_numero
end

Então(/^serei encaminhado para browser para baixar o boleto1$/) do
  page(Credito).botao_acessar_link
end

Quando(/^realizo o pagamento da fatura com o valor total por debito em conta$/) do
  page(Credito).botao_pagar_fatura
  page(Credito).botao_proximo
  page(Credito).botao_pagar_parcela
  page(Credito).botao_debito_em_conta
  page(Credito).botao_popup_confirmar
  page(Credito).digitar_senha_cartao(4)
end

Então(/^visualizo o copo de credito sem a fatura fechada$/) do
  page(Credito).liquido_pagamento_fatura
end

#Pagamento adiantado
Quando(/^realizo o pagamento adiantado total da fatura com o boleto$/) do
  page(Credito).fatura_atual
  page(Credito).botao_adiantar_pagamento
  page(Credito).adiantamento_botao_pagar_agora
  page(Credito).botao_gerar_boleto
end

Então(/^serei encaminhado para browser para baixar o boleto2$/) do
  page(Credito).botao_acessar_link
end

Quando(/^realizo o pagamento adiantado parcial da fatura com o boleto$/) do
  page(Credito).fatura_atual
  page(Credito).botao_adiantar_pagamento
  page(Credito).adiantamento_informar_valor_parcial
  page(Credito).adiantamento_botao_pagar_agora
  page(Credito).adiantamento_popup
  page(Credito).adiantamento_botao_gerar_boleto
end

Então(/^serei encaminhado para browser para baixar o boleto3$/) do
  page(Credito).botao_acessar_link
end

Quando(/^realizo o pagamento adiantado com o valor total da fatura por debito em conta$/) do
  page(Credito).fatura_atual
  page(Credito).botao_adiantar_pagamento
  page(Credito).adiantamento_botao_pagar_agora
  page(Credito).adiantamento_popup
  page(Credito).botao_debito_em_conta
  page(Credito).botao_popup_confirmar
  page(Credito).digitar_senha_cartao
end

Então(/^visualizo o copo de credito com fatura atual zerada$/) do
  page(Credito).liquido_pagamento_fatura
end

Quando(/^realizo o pagamento adiantado parcial da fatura por debito em conta$/) do
  page(Credito).fatura_atual
  page(Credito).botao_adiantar_pagamento
  page(Credito).adiantamento_informar_valor_parcial
  page(Credito).adiantamento_botao_pagar_agora
  page(Credito).adiantamento_popup
  page(Credito).botao_debito_em_conta
  page(Credito).botao_popup_confirmar
  page(Credito).digitar_senha_cartao
end

Então(/^visualizo o copo de credito com o restante da fatura atual$/) do
  page(Credito).liquido_pagamento_fatura
end

#Consultar faturas futuras
Quando(/^toco em faturas futuras$/) do
    page(Credito).faturas_futuras
end

Então(/^visualizo os detalhes das faturas futuras$/) do
    @valor = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_value')
    @vencimento = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_expires')
    @fechamento = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_closed')
    puts "Valor da fatura #{@valor}, Data de #{@vencimento}, #{@fechamento}"
end

#Consultar fatura atual
Quando(/^toco em fatura atual$/) do
    page(Credito).fatura_atual
end

Então(/^visualizo os detalhes da faturas atual$/) do
    @valor = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_value')
    @vencimento = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_expires')
    @fechamento = page(Comum).retornar_texto_do_elemento('credit_invoice_detail_txt_closed')
    puts "Valor da fatura atual é de #{@valor}, Data de #{@vencimento}, #{@fechamento}"
end

#Consultar saldo credor
Quando(/^o copo de faturas carregar$/) do
    page(Credito).resumo_de_credito
end

Então(/^visualizo o meu saldo credor$/) do
    @valor = page(Comum).retornar_texto_do_elemento('credit_invoice_list_txt_over_balance')
    puts "Valor do saldo credor é #{@valor}"
end
