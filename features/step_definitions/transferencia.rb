Dado(/^que acesse o menu de transfêrencias$/) do
  page(Transferencia).acessar_transferencia
  page(Transferencia).fechar_facebook
end

Quando(/^realizar uma transferência para o banco "([^"]*)"$/) do |banco|
  page(Transferencia).selecionar_favorecido(MASSA['cliente_transferencia'][banco])
  page(Transferencia).selecionar_conta(MASSA['bancos'][banco])
  page(Comum).validar_texto('Digite quanto você quer transferir:', 'qual é o valor?')
  page(Transferencia).informar_valor
  @valor_transf = page(Comum).retornar_texto_do_elemento('transfer_value_txt_value')
  @valor_envio = @valor_transf.sub('R$', '')
  page(Comum).botao_proximo
  @data = page(Comum).retornar_texto_do_elemento('tv_date')
  page(Comum).validar_texto('Transferir hoje', 'transferir quando?', 'Transferir este valor', '1', 'vez')
  puts "Data do dia #{@data}"
  page(Comum).botao_proximo
  page(Transferencia).scroll_up
  page(Comum).digitar_senha_cartao(4)
  page(Comum).aguardar_elemento_sumir("* id:'dialog_progress_txt_msg'")
end

Então(/^devo visualizar as informações do comprovante da transfência$/) do
  page(Comum).validar_texto('transferência feita')
  @valor = page(Comum).retornar_texto_do_elemento('tv_value')
  @fav = page(Comum).retornar_texto_do_elemento('lblSentToName')
  @banco = page(Comum).retornar_texto_do_elemento('lblBankName')
  @ag = page(Comum).retornar_texto_do_elemento('lblAgencyTo')
  @cc = page(Comum).retornar_texto_do_elemento('lblAccountTo')
  page(Comum).contem_texto(@data)
  page(Comum).contem_texto(@valor_envio)
  puts "Valor: #{@valor} / Favorecido: #{@fav} / Banco: #{@banco} / Agencia: #{@ag} / Conta: #{@cc}"
end

Quando(/^cadastrar um novo contato$/) do
  page(Transferencia).btn_adicionar_contato
  page(Transferencia).preencher_cpf_cnpj
  page(Comum).botao_proximo
  page(Transferencia).preencher_nome
  page(Comum).botao_proximo
  page(Comum).validar_texto('qual é o banco?')
  page(Transferencia).selecionar_banco('Bradesco')
  page(Transferencia).preencher_agencia('0001')
  page(Comum).botao_proximo
  page(Transferencia).preencher_conta('923940')
  page(Comum).botao_proximo
  page(Transferencia).informar_valor
  page(Comum).botao_proximo
  page(Comum).validar_texto('transferir quando?')
  page(Comum).botao_proximo
  wait_for_elements_exist(["* text:'*Transferências para outros bancos possuem \numa tarifa no valor de R$3,50.'"], timeout: 60)
  page(Transferencia).scroll_up
  page(Comum).digitar_senha_cartao(4)
  page(Comum).aguardar_elemento_sumir("* id:'dialog_progress_txt_msg'")
end

Quando(/^realizar um agendamento de transferência para o banco "([^"]*)"$/) do |banco|
  page(Transferencia).selecionar_favorecido(MASSA['cliente_transferencia'][banco])
  page(Transferencia).selecionar_conta(MASSA['bancos'][banco])
  page(Comum).validar_texto('Digite quanto você quer transferir:', 'qual é o valor?')
  page(Transferencia).informar_valor
  @valor_transf = page(Comum).retornar_texto_do_elemento('transfer_value_txt_value')
  @valor_envio = @valor_transf.sub('R$', '')
  page(Comum).botao_proximo
  page(Comum).validar_texto('Transferir hoje', 'transferir quando?', 'Transferir este valor', '1', 'vez')
  page(Transferencia).selecionar_data(2019, 12, 19)
  page(Comum).contem_texto(MENSAGENS['transferencia']['saldo_em_conta'])
  page(Comum).botao_proximo
  page(Comum).contem_texto(@valor_envio)
  @agendada = page(Comum).retornar_texto_do_elemento('rl_schedule')
  puts "#{@agendada}"
  page(Transferencia).scroll_up
  page(Comum).digitar_senha_cartao(4)
  page(Comum).aguardar_elemento_sumir("* id:'dialog_progress_txt_msg'")
end

Então(/^devo visualizar as informações do comprovante de agendamento$/) do
  @valor = page(Comum).retornar_texto_do_elemento('tv_value')
  @fav = page(Comum).retornar_texto_do_elemento('lblSentToName')
  @cpf = page(Comum).retornar_texto_do_elemento('lblCPF')
  @banco = page(Comum).retornar_texto_do_elemento('lblBankName')
  @ag = page(Comum).retornar_texto_do_elemento('lblAgencyTo')
  @cc = page(Comum).retornar_texto_do_elemento('lblAccountTo')
  @comp_agend = page(Comum).retornar_texto_do_elemento('lblReceiptNumber')
  page(Comum).contem_texto(@data)
  page(Comum).contem_texto(@valor_envio)
  puts "Comprovante: #{@comp_agend} / Valor: #{@valor} / Favorecido: #{@fav} / CPF: #{@cpf} / Banco: #{@banco} / Agencia: #{@ag} / Conta: #{@cc}"
end

Quando("realizar uma agendamento de transferencia para outro banco dia nao util {string}") do |banco|
  page(Transferencia).selecionar_favorecido(MASSA['cliente_transferencia'][banco])
  page(Transferencia).selecionar_conta(MASSA['bancos'][banco])
  page(Comum).validar_texto('Digite quanto você quer transferir:', 'qual é o valor?')
  page(Transferencia).informar_valor
  @valor_transf = page(Comum).retornar_texto_do_elemento('transfer_value_txt_value')
  @valor_envio = @valor_transf.sub('R$', '')
  page(Comum).botao_proximo
  page(Comum).validar_texto('Transferir hoje', 'transferir quando?', 'Transferir este valor', '1', 'vez')
  page(Transferencia).selecionar_data(2019, 12, 25)
  page(Comum).contem_texto(MENSAGENS['transferencia']['saldo_em_conta'])
  page(Comum).botao_proximo
  page(Transferencia).botao_sim_agendar
  page(Transferencia).botao_sim_transferir
  page(Comum).contem_texto(@valor_envio)
  @agendada = page(Comum).retornar_texto_do_elemento('rl_schedule')
  puts "#{@agendada}"
  page(Transferencia).scroll_up
  page(Comum).digitar_senha_cartao(4)
  page(Comum).aguardar_elemento_sumir("* id:'dialog_progress_txt_msg'")

end

Então("devo visualizar as informações do comprovante dia util") do
  @valor = page(Comum).retornar_texto_do_elemento('tv_value')
  @fav = page(Comum).retornar_texto_do_elemento('lblSentToName')
  @cpf = page(Comum).retornar_texto_do_elemento('lblCPF')
  @banco = page(Comum).retornar_texto_do_elemento('lblBankName')
  @ag = page(Comum).retornar_texto_do_elemento('lblAgencyTo')
  @cc = page(Comum).retornar_texto_do_elemento('lblAccountTo')
  @agpara = page(Comum).retornar_texto_do_elemento('lblReceiptDesc')
  @dataag = page(Comum).retornar_texto_do_elemento('lblReceiptDate')
  @comp_agend = page(Comum).retornar_texto_do_elemento('lblReceiptNumber')
  page(Comum).contem_texto(@data)
  page(Comum).contem_texto(@valor_envio)
  puts "Comprovante: #{@comp_agend} / Valor: #{@valor} / Favorecido: #{@fav} / CPF: #{@cpf} / Banco: #{@banco} / Agencia: #{@ag} / Conta: #{@cc} / Agendado para: #{@agpara} / Data agendado:#{@dataag}"
end

Quando("cadastrar uma nova conta de um contato já existente") do
  page(Transferencia).btn_cadastrados
  page(Transferencia).cadastrar_novo_banco
  page(Transferencia).selecionar_banco("Bradesco")
  agencia = Faker::Number.number(5)
  page(Transferencia).preencher_agencia(agencia)
  page(Comum).botao_proximo
  conta = Faker::Number.number(7)
  page(Transferencia).preencher_conta(conta)
  page(Comum).botao_proximo
  page(Transferencia).informar_valor
  page(Comum).botao_proximo
  page(Transferencia).validar_data_transferencia
  page(Transferencia).scroll_up
  page(Comum).digitar_senha_cartao(4)
end
