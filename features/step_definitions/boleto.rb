Dado(/^que acesse o menu de depósitos$/) do
  page(Deposito).acessar_deposito
end

Quando("realizar a geração de um boleto {string}") do |valor|
  page(Comum).swipe_direita('base_onboarding_page_indicator', 3)
  page(Comum).aguarda_elemento_aparecer(MENSAGENS['deposito']['msg_onboarding_neon_plus'])
  page(Comum).botao_entendi
  page(Comum).validar_texto('depósitos', 'Como você quer depositar dinheiro na sua conta Neon?', 'Depósito por boleto')
  page(Comum).validar_elemento('box_button_txt_description', 'box_button_txt_title', 'box_button_img')
  page(Deposito).btn_deposito
  page(Deposito).informar_valor_boleto(MASSA['boleto'][valor])
  page(Comum).validar_texto('Valor do boleto', 'Emissão de boletos Neon', 'Disponível no mês:')
  @disponivel = page(Comum).retornar_texto_do_elemento('daily_limits_txt_description')
  @total = page(Comum).retornar_texto_do_elemento('daily_limits_txt_total_value')
  puts "Valor Disponivel:#{@disponivel} / Valor Total:#{@total}"
  page(Comum).botao_proximo
end

Então(/^devo visualizar o boleto$/) do
  page(Comum).validar_texto('2 dias úteis', 'Esse é o prazo médio para o dinheiro cair na sua conta após o pagamento do boleto.')
  page(Comum).botao_entendi
  @num_boleto_neon = page(Comum).retornar_texto_do_elemento('deposit_billet_txt_bar_code')
  @venc = page(Comum).retornar_texto_do_elemento('deposit_billet_txt_due_date_value')
  @valor = page(Comum).retornar_texto_do_elemento('deposit_billet_txt_value')
  @email = page(Comum).retornar_texto_do_elemento('deposit_billet_txt_has_send_email')
  puts "Numero do boleto: #{@num_boleto_neon} / Vencimento: #{@venc} / Valor do depósito: #{@valor} / Email: #{@email}"
  press_back_button
  page(Comum).validar_texto('Depósito por boleto', 'Transferência bancária')
  press_back_button
end

Então(/^devo vizualizar a mensagem "([^"]*)"$/) do |mensagem|
  page(Comum).validar_texto("#{mensagem}")
end
