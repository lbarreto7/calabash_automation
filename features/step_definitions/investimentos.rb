Dado(/^que acesso o menu de Investimentos$/) do
  page(Investimentos).acessar_investimentos
  page(Comum).validar_elemento('default_onboard_txt_title')
  page(Comum).swipe_direita('base_onboarding_page_indicator', 2)
  page(Comum).botao_entendi
end

Quando("criar um novo objetivo {string}") do |name|
  page(Investimentos).botao_criar_objetivo
  page(Investimentos).escolher_investimento('Objetivo')
  page(Comum).botao_proximo
  page(Comum).validar_texto('sonhe alto :)', 'Valor final')
  page(Investimentos).informar_valor(1000)
  page(Comum).aguarda_elemento_aparecer("* text:'Você já sabe o que quer!\nQuanto dinheiro precisa juntar?'")
  page(Comum).botao_proximo
  page(Comum).validar_texto('parcelas', 'Escolha quanto quer investir por mês até alcançar seu objetivo.')
  page(Investimentos).informar_valor(100)
  page(Comum).botao_proximo
  page(Investimentos).final_straight_screen
  page(Comum).validar_texto('crie um nome')
  page(Comum).validar_texto('Como quer chamar o seu Objetivo?')
  page(Comum).validar_texto('Nome')
  page(Investimentos).informar_nome(name)
  page(Comum).botao_proximo
  page(Investimentos).selecionar_imagem_neon
  page(Comum).botao_proximo
  page(Comum).validar_texto('1.000')
  page(Comum).scroll_ate_elemento_aparecer("* text:'CRIAR INVESTIMENTO'")
  page(Comum).validar_texto("#{name}")
  page(Comum).botao_proximo
end

Quando(/^criar livre$/) do
  page(Investimentos).botao_criar_objetivo
  page(Investimentos).escolher_investimento('Livre')
  page(Comum).botao_proximo
  page(Comum).validar_texto('valor', 'Valor inicial')
  page(Investimentos).informar_valor(1000)
  page(Comum).validar_texto('CRIAR INVESTIMENTO')
  page(Comum).botao_proximo
end

Quando(/^realizar um deposito parcial$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Deposito parcial'")
  page(Investimentos).acessar_deposito_investir
  page(Investimentos).informar_valor_investimento(100)
  page(Comum).validar_texto('100')
  page(Investimentos).botao_investir
  page(Comum).aguarda_elemento_aparecer("* text:'Valor do investimento\nR$ 100,00'")
  page(Investimentos).realizar_deposito
end

Quando(/^realizar um deposito total$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Total'")
  page(Investimentos).acessar_deposito_investir
  page(Investimentos).informar_valor_investimento(1000)
  page(Comum).validar_texto('1.000')
  page(Investimentos).botao_investir
  $valor_total = page(Comum).aguarda_elemento_aparecer("* text:'Valor do investimento\nR$ 1.000,00'")
  page(Investimentos).realizar_deposito
end

Quando(/^resgatar parcialmente um objetivo$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Resgate parcial'")
  @valor_para_resgate = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_total_value')
  page(Investimentos).acessar_deposito_resgatar
  page(Comum).validar_texto(@valor_para_resgatee)
  page(Investimentos).informar_valor_resgate(10)
  page(Investimentos).botao_continuar
  page(Investimentos).quebrar_porquinho(6)
end

Quando(/^resgatar o valor total do objetivo$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Total'")
  page(Investimentos).acessar_deposito_resgatar
  page(Investimentos).quebrar_porquinho(6)
end

Então(/^devo visualizar o objetivo resgatado$/) do
  page(Comum).validar_texto('detalhes')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_alvo = page(Comum).retornar_texto_do_elemento('investment_detail_txt_target_value')
  @valor_investido = page(Comum).retornar_texto_do_elemento('investment_detail_txt_deposited_value')
  page(Comum).scroll_down(2)
  @rendimento = page(Comum).retornar_texto_do_elemento('investment_detail_txt_income_value')
  @data_inicial = page(Comum).retornar_texto_do_elemento('investment_detail_txt_start_date_value')
  @data_final = page(Comum).retornar_texto_do_elemento('investment_detail_txt_end_date_value')
  puts "Nome do Investimento:: #{@nome}, Valor Alvo: #{@valor_alvo}, Valor investido: #{@valor_investido}, Rendimento: #{@rendimento}, Data Inicial: #{@data_inicial}, Data Final: #{@data_final}"
end

Então(/^devo visualizar o objetivo criado$/) do
  page(Comum).validar_texto('Valor total')
  page(Comum).validar_texto('R$ 0,00')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_total = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_total_value')
  @ja_rendeu = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_income_value')
  puts "Nome do Investimento: #{@nome}, Valor total: #{@valor_total}, Já rendeu: #{@ja_rendeu}"
end

Então(/^devo visualizar o investimento criado$/) do
  page(Comum).validar_texto('R$ 1.000,00')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_total = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_total_value')
  @ja_rendeu = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_income_value')
  puts "Nome do Investimento: #{@nome}, Valor total: #{@valor_total}, Já rendeu: #{@ja_rendeu}"
end

Então(/^devo visualizar o objetivo concluído$/) do
  page(Comum).validar_texto('Total')
  page(Comum).validar_texto('Valor total')
  page(Comum).validar_texto('Já rendeu')
  page(Comum).validar_texto('RESGATAR TUDO')
  page(Comum).validar_texto('R$ 1.000,00')
  page(Comum).validar_texto('Valor alvo')
  page(Comum).validar_texto('Valor investido')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor = page(Comum).retornar_texto_do_elemento('investment_detail_txt_deposited_value')
  @ja_rendeu = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_income_value')
  page(Comum).scroll_down(2)
  @data_inicial = page(Comum).retornar_texto_do_elemento('investment_detail_txt_start_date_value')
  @data_final = page(Comum).retornar_texto_do_elemento('investment_detail_txt_end_date_value')
  puts "Nome do Investimento: #{@nome}, Valor Total Alcançado: #{@valor}, Rendimento: #{@ja_rendeu}, Data Inicial: #{@data_inicial}, Data Final: #{@data_final}"
  press_back_button
  page(Comum).validar_texto('Total')
  page(Comum).scroll_down(1)
  page(Comum).validar_texto('Valor total')
  page(Comum).validar_texto("#{@valor}")
  page(Comum).validar_texto('Já rendeu')
  page(Comum).validar_texto('R$ 0,00')
  press_back_button
  page(Investimentos).acessar_investimentos
  page(Investimentos).show_flow_objetivo_concluido
end

Então(/^devo visualizar o valor parcial depositado no objetivo$/) do
  page(Comum).validar_texto('detalhes')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_alvo = page(Comum).retornar_texto_do_elemento('investment_detail_txt_target_value')
  @valor_investido = page(Comum).retornar_texto_do_elemento('investment_detail_txt_deposited_value')
  page(Comum).scroll_down(2)
  @rendimento = page(Comum).retornar_texto_do_elemento('investment_detail_txt_income_value')
  @data_inicial = page(Comum).retornar_texto_do_elemento('investment_detail_txt_start_date_value')
  @data_final = page(Comum).retornar_texto_do_elemento('investment_detail_txt_end_date_value')
  puts "Nome do Investimento: #{@nome}, Valor Alvo: #{@valor_alvo}, Valor investido: #{@valor_investido}, Rendimento: #{@rendimento}, Data Inicial: #{@data_inicial}, Data Final: #{@data_final}"
end

Então(/^devo visualizar o objetivo resgatado total$/) do
  page(Comum).aguardar_elemento_sumir('Total')
end

Quando(/^realizar um depósito igual ao valor mínimo$/) do
    page(Investimentos).acessar_detalhe_obj("* text:'Livre'")
    page(Investimentos).acessar_deposito_investir
    page(Investimentos).informar_valor_investimento(100)
    page(Comum).validar_texto('100')
    page(Investimentos).botao_investir
    page(Comum).aguarda_elemento_aparecer("* text:'Valor do investimento\nR$ 100,00'")
    page(Investimentos).realizar_deposito
end

Então(/^devo visualizar que o valor foi creditado ao investimento livre$/) do
  page(Comum).validar_texto('detalhes')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_total = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_total_value')
  @valor_investido = page(Comum).retornar_texto_do_elemento('investment_detail_txt_deposited_value')
  page(Comum).scroll_down(2)
  @rendimento = page(Comum).retornar_texto_do_elemento('investment_detail_txt_income_value')
  @data_inicial = page(Comum).retornar_texto_do_elemento('investment_detail_txt_start_date_value')
  puts "Nome do Investimento: #{@nome}, Valor total: #{@valor_total}, Valor investido: #{@valor_investido}, Rendimento: #{@rendimento}, Data Inicial: #{@data_inicial}"
end

Quando(/^realizar um depósito menor que o valor mínimo$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Depósito livre inválido'")
  page(Investimentos).acessar_deposito_investir
  page(Investimentos).informar_valor_investimento(9)
  page(Comum).validar_texto('9')
end

Então(/^devo visualizar uma mensagem informando qual o valor mínimo para aplicações em livre$/) do
  @msg = page(Comum).validar_texto(MENSAGENS['objetivos']['msg_valor_invalido'])
  puts @msg[0]
end

Quando(/^realizar o resgate de um valor parcial do livre$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Depósito parcial livre'")
  page(Investimentos).acessar_deposito_resgatar
  page(Investimentos).informar_valor_resgate(12)
  page(Investimentos).botao_continuar
  page(Investimentos).quebrar_porquinho(6)
end

Quando(/^realizar o resgate total do livre$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Livre'")
  page(Investimentos).acessar_deposito_resgatar
  page(Investimentos).btn_valor_total
  page(Investimentos).botao_continuar
  page(Investimentos).quebrar_porquinho(6)
end

Então(/^devo visualizar que o valor foi debitado do investimento livre$/) do
  page(Comum).validar_texto('detalhes')
  @nome = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_title')
  @valor_total = page(Comum).retornar_texto_do_elemento('item_investment_card_txt_total_value')
  @valor_investido = page(Comum).retornar_texto_do_elemento('investment_detail_txt_deposited_value')
  page(Comum).scroll_down(2)
  @rendimento = page(Comum).retornar_texto_do_elemento('investment_detail_txt_income_value')
  @data_inicial = page(Comum).retornar_texto_do_elemento('investment_detail_txt_start_date_value')
  puts "Nome do Investimento: #{@nome}, Valor total: #{@valor_total}, Valor investido: #{@valor_investido}, Rendimento: #{@rendimento}, Data Inicial: #{@data_inicial}"
end

Quando(/^editar o nome de um investimento livre$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Livre'")
  page(Investimentos).btn_editar
  page(Investimentos).alterar_texto
  page(Comum).botao_proximo
end

Então(/^devo visualizar o investimento com o nome alterado$/) do
  page(Comum).validar_texto('detalhes')
  page(Comum).validar_texto($nome)
end

Quando(/^editar o nome de um objetivo$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'R$ 100,00 por mês'")
  page(Investimentos).btn_editar
  page(Investimentos).alterar_texto
  page(Comum).botao_proximo
end

Quando(/^excluir um objetivo$/) do
  page(Investimentos).acessar_detalhe_obj("* text:'Excluir'")
  page(Comum).validar_texto('detalhes')
  page(Investimentos).btn_excluir
  page(Comum).validar_texto('excluir investimento?')
  page(Comum).validar_texto('O valor total alcançado neste investimento será transferido para sua conta. Após deduzidos os impostos.')
  page(Comum).validar_texto('Você depositou')
  @voce_depositou = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_deposit_value')
  page(Comum).validar_texto('Rendimentos ganhos')
  @rendimentos_ganhos = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_income_value')
  page(Comum).validar_texto('Impostos (IR)')
  @inpostos_ir = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_taxes_value')
  page(Comum).validar_texto('Valor do resgate')
  @valor_do_resgate = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_liquido_value')
  puts "Você depositou #{@voce_depositou}, Rendimentos ganhos #{@rendimentos_ganhos}, Impostos (IR) #{@inpostos_ir}, Valor do resgate #{@valor_do_resgate} "
  page(Investimentos).btn_sim_excluir
end
Quando("excluir um investimento livre") do
  step 'criar livre'
  page(Investimentos).acessar_detalhe_obj("* text:'Livre'")
  page(Comum).validar_texto('detalhes')
  page(Investimentos).btn_excluir
  page(Comum).validar_texto('excluir investimento?')
  page(Comum).validar_texto('O valor total alcançado neste investimento será transferido para sua conta. Após deduzidos os impostos.')
  page(Comum).validar_texto('Você depositou')
  @voce_depositou = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_deposit_value')
  page(Comum).validar_texto('Rendimentos ganhos')
  @rendimentos_ganhos = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_income_value')
  page(Comum).validar_texto('Impostos (IR)')
  @inpostos_ir = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_txt_taxes_value')
  page(Comum).validar_texto('Valor do resgate')
  @valor_do_resgate = page(Comum).retornar_texto_do_elemento('dialog_goal_delete_liquido_value')
  puts "Você depositou #{@voce_depositou}, Rendimentos ganhos #{@rendimentos_ganhos}, Impostos (IR) #{@inpostos_ir}, Valor do resgate #{@valor_do_resgate} "
  page(Investimentos).btn_sim_excluir
end

Então(/^não devo visualizar o objetivo$/) do
  page(Comum).validar_texto('Excluir')
end

Então(/^não devo visualizar o livre$/) do
  page(Comum).aguardar_elemento_sumir("* text:'Livre'")
end
