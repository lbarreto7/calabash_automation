Dado(/^que realizar login$/) do
  page(Comum).validar_texto('meu neon', 'saldo', 'investimentos')
end

Quando(/^acessar o saldo total de despesas do mês$/) do
  page(Comum).validar_texto('meu neon', 'saldo', 'investimentos')
  page(Saldo).acessar_saldo
end

Então(/^devo visualizar as despesas do mês$/) do
  @saldo = page(Comum).retornar_texto_do_elemento('balance_txt_value')
  @total_dep = page(Comum).retornar_texto_do_elemento('tv_value')
  @trans = page(Comum).retornar_texto_do_elemento('tv_description')
  @valor_trans = page(Comum).retornar_texto_do_elemento('tv_value', 2)
  @data = page(Comum).retornar_texto_do_elemento('tv_date')
  puts "Saldo total: #{@saldo}, Total de despesas: #{@total_dep}, Tipo de transação: #{@trans}"
  puts "Valor da transação: #{@valor_trans}, Data da transação: #{@data}"
end

Dado(/^que acesse o saldo futuro$/) do
  page(Saldo).acessar_saldo
  page(Saldo).acessar_saldo_futuros
end

Quando(/^acessar o saldo de despesas anteriores$/) do
  page(Saldo).acessar_saldo
  page(Saldo).acessar_saldo_enteriores
end

Quando(/^realizar a exclusão do agendamento$/) do
  page(Saldo).acessar_item('NEON PAGAMENTOS S.A.')
  page(Saldo).excluir_item
end

Quando(/^acessar o saldo de despesas futuro$/) do
  page(Saldo).acessar_saldo
  page(Saldo).acessar_saldo_futuros
end

Então(/^devo visualizar as despesas futuras da conta$/) do
  page(Comum).aguardar_elemento_sumir("* text:'carregando dados...'")
  page(Comum).validar_texto('Futuros', 'Total agendado')
  @saldo = page(Comum).retornar_texto_do_elemento('balance_txt_value')
  @total_dep = page(Comum).retornar_texto_do_elemento('tv_value')
  @trans = page(Comum).retornar_texto_do_elemento('tv_description')
  @valor_trans = page(Comum).retornar_texto_do_elemento('tv_value', 2)
  @data = page(Comum).retornar_texto_do_elemento('tv_date')
  puts "Saldo total: #{@saldo}, Total de despesas: #{@total_dep}, Tipo de transação: #{@trans}"
  puts "Valor da transação: #{@valor_trans}, Data da transação: #{@data}"
end

Quando(/^realizo uma agendamento de pagamento$/) do
  page(Saldo).acessar_saldo
  page(Saldo).acessar_saldo_futuros
  page(Saldo).acessar_item('NEON PAGAMENTOS S.A.')
end

Então(/^devo visualizar os detalhes do agendamento$/) do
  page(Saldo).acessar_comprovante
  @protocolo = page(Comum).retornar_texto_do_elemento('receipt_tv_protocol')
  @nome = page(Comum).retornar_texto_do_elemento('receipt_tv_name')
  @data = page(Comum).retornar_texto_do_elemento('receipt_tv_payment_date_value')
  @valor = page(Comum).retornar_texto_do_elemento('receipt_tv_payment_value')
  @descricao = page(Comum).retornar_texto_do_elemento('receipt_tv_description')
  @codigo_de_barras = page(Comum).retornar_texto_do_elemento('receipt_tv_bar_code')
  puts "Protocolo: #{@protocolo}, Nome do cliente: #{@nome}, Dia: #{@data} , Valor do boleto: #{@valor}, Descrição: #{@descricao}, Código de barras: #{@codigo_de_barras}"
end

Dado(/^que acesse o saldo do mês corrente$/) do
  step 'que acesse o aplicativo Neon'
  step 'realizar o login com um "cliente_pf"'
  page(Comum).validar_texto('saldo')
  page(Saldo).acessar_saldo
end

Quando(/^acessar o detalhe de uma transação$/) do
  page(Comum).validar_texto('Total de despesas')
  @saldo = page(Comum).retornar_texto_do_elemento('tv_value')
  @hora = page(Comum).retornar_texto_do_elemento('tv_date')
  page(Saldo).acessar_item('Enviou')
end

Então(/^devo visualizar os detalhes da transação$/) do
  page(Comum).validar_elemento('toolbar_title', 'transaction_detail_txt_value', 'transaction_detail_txt_name', 'transaction_detail_btn_category')
  @validar_saldo = @saldo.sub('-', "")
  @validar_hora = @hora.sub('hoje às ', '')
  page(Comum).contem_texto(@validar_saldo)
  page(Comum).contem_texto(@validar_hora)
  puts "Valor: #{@validar_saldo}, Data da Transação: #{@validar_hora}"
end

Quando(/^realizar a exclusão de uma transferencia agendada$/) do
  page(Saldo).acessar_item('Enviar para')
  page(Saldo).excluir_item
end

Então(/^não devo mais visualizar o agendamento da conta$/) do
  page(Comum).validar_texto('Futuros', 'Total agendado')
  @saldo = page(Comum).retornar_texto_do_elemento('balance_txt_value')
  @total_dep = page(Comum).retornar_texto_do_elemento('tv_value')
  @trans = page(Comum).retornar_texto_do_elemento('tv_description')
  @valor_trans = page(Comum).retornar_texto_do_elemento('tv_value', 2)
  @data = page(Comum).retornar_texto_do_elemento('tv_date')
  puts "Saldo total: #{@saldo}, Total de despesas: #{@total_dep}, Tipo de transação: #{@trans}"
  puts "Valor da transação: #{@valor_trans}, Data da transação: #{@data}"
end

Então("visualizo comprovante de agendamento de pagamento") do
  page(Saldo).acessar_item('NEON PAGAMENTOS S.A.')
  page(Saldo).acessar_comprovante
  page(Comum).validar_texto('PAGAMENTO AGENDADO PARA:', 'Descrição:', 'Data de vencimento:', 'Valor do pagamento:')
  @data  = page(Comum).retornar_texto_do_elemento('receipt_tv_due_date_value')
  @descricao = page(Comum).retornar_texto_do_elemento('receipt_tv_description')
  @valor = page(Comum).retornar_texto_do_elemento('receipt_tv_payment_value')
  @codigo_de_barras = page(Comum).retornar_texto_do_elemento('receipt_tv_bar_code')
  @numero_doc = page(Comum).retornar_texto_do_elemento('receipt_tv_document_number')
  puts "Data Agendada para Pagamento: #{@data}, Pagador: #{@descricao},
  Valor: #{@valor}, Codigo de barras: #{@codigo_de_barras}, Numero do documento: #{@numero_doc}"
end
