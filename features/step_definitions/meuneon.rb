Dado(/^que acesso a area do meu neon$/) do
  page(Meu_neon).acessar_meu_neon
  page(Comum).validar_texto('meu neon', 'Agência: 0655', 'COMPARTILHAR', 'MINHA CONTA', 'meu neon+')
  page(Comum).scroll_ate_elemento_aparecer("* text:'sair'")
  page(Comum).validar_texto('sobre', 'benefícios e tarifas', 'configurações')
end


Quando("acessar solicitar documentos") do
  page(Comum).scroll_ate_elemento_aparecer("* text:'solicitar documentos'")
  page(Meu_neon).acessar_item_menu('solicitar documentos')
  page(Comum).validar_texto('termo de transferência', 'informe de tarifas', 'extrato do período', 'comprovante de titularidade', 'extrato de investimentos')
  page(Comum).scroll_ate_elemento_aparecer("* text:'nota de resgate'")
  page(Comum).validar_texto('extrato do crédito legado', 'informe de rendimentos', 'nota de resgate')
end

Quando(/^acessar email "([^"]*)"$/) do |cliente|
  page(Meu_neon).acessar_item_menu('configurações')
  page(Comum).validar_texto('configurações', 'dados pessoais', 'endereço de entrega', 'e-mail', 'senha do app', 'senha do cartão')
  page(Comum).validar_texto('limites', 'sincronizar facebook', 'cancelar conta')
  page(Meu_neon).novo_email
  page(Comum).validar_texto('novo email', 'Insira o e-mail que deseja vincular à sua conta Neon.')
  page(Meu_neon).digitar_email(MASSA['clientes'][cliente])
  page(Meu_neon).confirmar_email
end

Então("devo inserir o novo e-mail com sucesso") do
  page(Comum).validar_texto('senha selfie')
end

Quando("acessar ajuda") do
  page(Meu_neon).acessar_item_menu('ajuda')
end

Então("devo visualizar a faq com a perguntas frequentes") do
  page(Comum).scroll_ate_elemento_aparecer("* text:'→ Cadastro'")
  page(Comum).validar_texto('→ Cadastro', '→ Ativação da conta', '→ Recargas')
  tap_when_element_exists("* text:'→ Cadastro'")
  page(Comum).validar_texto('→ Como faço para abrir uma conta?')
  tap_when_element_exists("* text:'→ Como faço para abrir uma conta?'")
  tap_when_element_exists("* id:'action_chat'")
  page(Comum).validar_texto('chat')
end

Quando("acessar benefícios e tarifas") do
  page(Meu_neon).acessar_item_menu('benefícios e tarifas')
end


Então("devo visualizar as tarifas e benefícos") do
  page(Comum).validar_texto('benefícios e tarifas', 'benefícios disponíveis', 'saque gratuito', 'emissão de boleto', 'Gratuito','transferências gratuitas')
  page(Comum).scroll_ate_elemento_aparecer("* text:'compras internacionais'")
  page(Comum).validar_texto('tarifas', 'demais depósitos via boleto')
  @saque = page(Comum).retornar_texto_do_elemento('neon_list_item_txt_end_text')
  @boleto = page(Comum).retornar_texto_do_elemento('neon_list_item_txt_end_text', 1)
  @trans = page(Comum).retornar_texto_do_elemento('neon_list_item_txt_end_text', 2)
  puts "Saque: #{@saque} / Boletos: #{@boleto} / Transferencia: #{@trans}"
end

Quando("acessar sobre") do
  page(Meu_neon).acessar_item_menu('sobre')
end

Então("devo visualizar as informações sobre o app") do
  page(Comum).validar_texto('sobre', 'ATUALIZAR VERSÃO', 'termos de uso', 'política de privacidade', 'tudo o que você precisa saber', 'o que coletamos e por quê')
  @versao = page(Comum).retornar_texto_do_elemento('about_txt_version')
  puts "Versão do app: #{@versao}"
end

Quando("realizar o logout") do
  page(Meu_neon).acessar_item_menu('sair')
  page(Meu_neon).sair_app
end

Então("devo visualizar o logout do app") do
  page(Comum).validar_texto('senha numérica')
  @user = page(Comum).retornar_texto_do_elemento('new_password_message')
  puts "Usuário: #{@user}"
end

Então("devo solicitar o informe de rendimentos com sucesso") do
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'informe de rendimentos'")
  page(Meu_neon).acessar_item_menu('informe de rendimentos')
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
  page(Comum).validar_texto('solicitar documentos')
end

Então("devo solicitar o informe de tarifas com sucesso") do
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'informe de tarifas'")
  page(Meu_neon).acessar_item_menu('informe de tarifas')
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
  page(Comum).validar_texto('solicitar documentos')
end

Então("devo solicitar o comprovante de titularidade") do
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'comprovante de titularidade'")
  page(Meu_neon).acessar_item_menu('comprovante de titularidade')
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
  page(Comum).validar_texto('solicitar documentos')
end

Então("devo solicitar o extrato do crédito legado") do
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'extrato do crédito legado'")
  page(Meu_neon).acessar_item_menu('extrato do crédito legado')
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
  page(Comum).validar_texto('solicitar documentos')
end

Então("devo solicitar informe de rendimentos") do
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'informe de rendimentos'")
  page(Meu_neon).acessar_item_menu('informe de rendimentos')
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
  page(Comum).validar_texto('solicitar documentos')
end

Então(/^devo solicitar o extrato do periodo de "([^"]*)" com sucesso$/) do |periodo|
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'extrato do período'")
  page(Meu_neon).acessar_item_menu('extrato do período')
  page(Meu_neon).acessar_item_menu(periodo)
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
end

Então(/^devo solicitar o extrato de investimento dos "([^"]*)" com sucesso$/) do |periodo|
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'extrato de investimentos'")
  page(Meu_neon).acessar_item_menu('extrato de investimentos')
  page(Comum).validar_texto("#{periodo}")
  page(Meu_neon).acessar_item_menu(periodo)
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
end

Dado(/^que acesso o extrato de investimento de um "([^"]*)"$/) do |periodo|
  page(Meu_neon).acessar_meu_neon
  page(Comum).scroll_ate_elemento_aparecer("* text:'solicitar documentos'")
  page(Meu_neon).acessar_item_menu('solicitar documentos')
  page(Comum).validar_texto('solicitar documentos')
  page(Comum).scroll_ate_elemento_aparecer_up("* text:'extrato de investimentos'")
  page(Meu_neon).acessar_item_menu('extrato de investimentos')
  page(Meu_neon).acessar_item_menu('período específico')

end

Quando(/^solicitar o extrato do ano "([^"]*)"$/) do |periodo|
  page(Meu_neon).acessar_item_menu(periodo)

end

Então(/^devo solicitar o extrato do mês "([^"]*)" com sucesso$/) do |periodo|
  page(Meu_neon).acessar_item_menu(periodo)
  page(Meu_neon).validar_email_enviado
  page(Meu_neon).enviar_email
end
