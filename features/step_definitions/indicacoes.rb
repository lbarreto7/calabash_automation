Dado("que acesso minhas indicacoes") do
  page(Indicacoes).acessar_indicacoes

  page(Comum).validar_texto('convide sua turma!')
  page(Comum).validar_texto_com_quebra('Compartilhe seu código único com quem quiser. Os 5 primeiros que se cadastrarem com ele e fizerem um depósito inicial recebem um presente da Neon, e você também!')
end

Quando("passar pelo tutorial") do
  page(Comum).validar_texto('CONVIDAR')
  page(Comum).botao_entendi
end

Então("devo visualizar todas as indicacoes com sucesso") do
  page(Comum).validar_texto('indicações', 'Seu código Neon', 'COMPARTILHAR')
  page(Comum).validar_elemento('action_info', 'referral_status_txt_message', 'referral_status_txt_code')
  page(Comum).swipe_direita('referral_avatar_txt_placeholder', 8)
  @indicacao = page(Comum).retornar_texto_do_elemento('referral_status_txt_code')
  puts "Código de Indicação: #{@indicacao}"
end

Então("devo visualizar o botao compartilhar com sucesso") do
  page(Comum).validar_texto('COMPARTILHAR')
  page(Comum).botao_entendi
  page(Comum).validar_texto('WhatsApp', 'Messenger', 'E-mail', 'Outros')
end

Quando("acessar detalhes") do
  page(Comum).validar_texto('CONVIDAR')
  page(Comum).botao_entendi
  page(Indicacoes).btn_detalhes
end

Então("devo visualizar os status dos convites com sucesso") do
  page(Comum).validar_texto('detalhes', 'status dos convites', 'cadastro iniciado', 'aguardando aprovação')
  page(Comum).validar_texto('aguardando deposito inicial', 'conta ativada', 'condições de uso', 'como o programa de indicações funciona')
  @status = page(Comum).retornar_texto_do_elemento('item_benefit_txt_value')
  puts "Status de Indicação: #{@status}"
end

Então("devo visualizar o termo e as condicoes com sucesso") do
  page(Comum).validar_texto('status dos convites')
  page(Indicacoes).btn_condicoes_uso
  page(Comum).validar_texto('condições de uso', 'ENTENDI')
  page(Comum).validar_elemento('simple_button_bg', 'simple_button_txt_text', 'term_btn_understand', 'statusBarBackground', 'toolbar')
  @titulo = page(Comum).retornar_texto_do_elemento('toolbar_title' )
  page(Comum).botao_entendi
  page(Comum).validar_texto('status dos convites', 'detalhes', 'condições de uso')
  page(Comum).validar_elemento('toolbar_title', 'referral_benefits_ctn_terms', 'item_benefit_txt_title', 'item_benefit_txt_value')
  puts "Titulo da tela: #{@titulo}"
end
