Dado(/^que acesso meus cartões$/) do
  page(Cartao).acessar_cartao
end

Quando(/^acessar o cartão virtual$/) do
  page(Comum).validar_texto('cartões', 'virtual', 'físico')
  page(Cartao).acessar_cartao_virtual
end

Então(/^devo visualizar todas as informações do cartão$/) do
  page(Comum).validar_texto('Cartão Ativo','COPIAR NÚMERO')
  @nome = page(Comum).retornar_texto_do_elemento('card_txt_name')
  @numero_cart_1 = page(Comum).retornar_texto_do_elemento('card_txt_one')
  @numero_cart_2 = page(Comum).retornar_texto_do_elemento('card_txt_two')
  @numero_cart_3 = page(Comum).retornar_texto_do_elemento('card_txt_three')
  @numero_cart_4 = page(Comum).retornar_texto_do_elemento('card_txt_four')
  @data_venc = page(Comum).retornar_texto_do_elemento('card_txt_date')
  @cvv = page(Comum).retornar_texto_do_elemento('card_txt_cvv')
  @cobranca = page(Comum).retornar_texto_do_elemento('virtual_card_txt_card_type')
  @texto_cat = page(Comum).retornar_texto_do_elemento('virtual_card_txt_description')
  puts "Número Cartão:#{@numero_cart_1} #{@numero_cart_2} #{@numero_cart_3} #{@numero_cart_4} / Nome Cliente:#{@nome} Data de Vencimento:#{@data_venc} / CVV:#{@cvv} / #{@cobranca} / Mensagem:#{@texto_cat}"
end

Então(/^devo visualizar todas as informações do cartão de crédito$/) do
  @nome = page(Comum).retornar_texto_do_elemento('card_txt_name')
  @numero_cart_1 = page(Comum).retornar_texto_do_elemento('card_txt_one')
  @numero_cart_2 = page(Comum).retornar_texto_do_elemento('card_txt_two')
  @numero_cart_3 = page(Comum).retornar_texto_do_elemento('card_txt_three')
  @numero_cart_4 = page(Comum).retornar_texto_do_elemento('card_txt_four')
  puts "Número Cartão:#{@numero_cart_1} #{@numero_cart_2} #{@numero_cart_3} #{@numero_cart_4} / Nome Cliente:#{@nome}"
end

Quando(/^acessar o cartão virtual de crédito$/) do
  page(Comum).validar_texto('cartões', 'virtual', 'físico')
  page(Cartao).acessar_cartao_virtual
  page(Cartao).botao_trocar_cartao
end

Quando(/^ativar o bloqueio temporario do cartão virtual$/) do
  page(Comum).validar_texto('cartões', 'virtual', 'físico')
  page(Cartao).acessar_cartao_virtual
  page(Comum).validar_texto('Cartão Ativo')
  page(Cartao).ativar_desativar_bloqueio_virtual
  page(Comum).validar_texto('Tem certeza que quer bloquear esse cartão temporariamente?', 'bloqueio temporário')
  page(Comum).botao_sim
end

Quando(/^desativar o bloqueio temporario do cartão virtual$/) do
  page(Cartao).acessar_cartao_virtual
  page(Comum).validar_texto('Bloqueio Temporário')
  page(Cartao).ativar_desativar_bloqueio_virtual
  page(Comum).validar_texto('Deseja desbloquear seu cartão virtual Neon?')
  page(Comum).botao_sim
end

Quando(/^ativar o bloqueio temporario do cartão físico$/) do
  page(Comum).validar_texto('cartões', 'virtual', 'físico')
  page(Comum).validar_texto('bloqueio temporário')
  page(Cartao).ativar_desativar_bloqueio_fisico
  page(Comum).validar_texto('Tem certeza que quer bloquear esse cartão temporariamente?', 'bloqueio temporário')
  page(Comum).botao_sim
end

Quando(/^desativar o bloqueio temporario do cartão físico$/) do
  page(Comum).validar_texto('cartões', 'virtual', 'físico')
  page(Comum).validar_texto('Bloqueio Temporário')
  page(Cartao).ativar_desativar_bloqueio_fisico
  page(Comum).validar_texto('Deseja desbloquear seu cartão físico Neon?')
  page(Comum).botao_sim
end

Então(/^devo visualizar o cartão bloqueado temporariamente$/) do
  page(Comum).validar_texto('Bloqueio Temporário')
  page(Comum).aguarda_elemento_aparecer(MENSAGENS['cartao']['bloqueio'])
end

Então(/^devo visualizar o cartão desbloqueado$/) do
  page(Comum).validar_texto('Cartão Ativo')
  page(Comum).aguarda_elemento_aparecer(MENSAGENS['cartao']['debito_virtual'])
end

Então(/^devo visualizar o cartão físico desbloqueado$/) do
  page(Comum).validar_texto('Cartão de Débito Ativo')
  page(Comum).aguarda_elemento_aparecer(MENSAGENS['cartao']['debito_fisico'])
end

Dado("que acesse o meus cartoes com um usuario com cartao fisico no status entregue") do
  step 'que acesse o aplicativo Neon'
  page(Cartao).acessar_menu_rastreio
end

Quando("solicitar a ativação do cartão fisico") do
  page(Comum).validar_texto('físico', 'virtual', 'RECEBI MEU CARTÃO')
  touch("* text:'RECEBI MEU CARTÃO'")
  page(Cartao).informar_codigo('tx_card_number', MASSA['cartoes']['numero'] )
  page(Cartao).informar_codigo('tx_card_cvv', MASSA['cartoes']['cvv'] )
  page(Comum).validar_texto('Para ativar o seu cartão, basta digitar os seguintes dados:', 'PRÓXIMO')
  touch("* id:'simple_button_txt_text'")
  page(Credito).digitar_senha_cartao(4)
  page(Comum).validar_texto("Repita a senha inserida anteriormente")
  page(Credito).digitar_senha_cartao(4)
end

Então("o cartao deve ser ativado com sucesso") do
  page(Comum).validar_texto("avaliação")
  touch("* id:'rating_input_btn_close'")
  page(Comum).validar_elemento('card_img')
  page(Comum).validar_texto(MASSA['cartao']['debito_fisico'])
end