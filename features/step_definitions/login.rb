Dado(/^que acesse o aplicativo Neon$/) do
  (ENV['ENV']) == 'DEV'
    puts 'AMBIENTE  - DEV'
    page(Login).on_boarding_login
    page(Login).botao_entrar
end

Quando(/^realizar o login com um "(.*?)"$/) do |cliente|
  page(Login).digitar_email(MASSA['clientes'][cliente])
  page(Comum).validar_texto('Tire dúvidas relacionadas à sua conta Neon.')
  page(Comum).validar_texto_com_quebra('Veja todas as notícias mais recentes direto do time Neon.')
  page(Login).botao_continuar
  page(Login).digitar_senha(6)
  page(Comum).aguardar_animacao_de_elemento('dashboard_ctn_main')
end

Então(/^devo visualizar a home do aplicativo$/) do
  @saldo = page(Comum).retornar_texto_do_elemento('dashboard_txt_balance_value')
  @objetivos = page(Comum).retornar_texto_do_elemento('dashboard_txt_goals_value')
  page(Comum).validar_texto('meu neon', 'saldo', 'investimentos', 'cartões')
  page(Comum).aguardar_animacao_de_elemento('dashboard_ctn_scroll')
  page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
  page(Comum).validar_texto('transferências', 'pagamentos', 'recargas')
  puts "Saldo #{@saldo}, investimentos #{@objetivos}"
end
