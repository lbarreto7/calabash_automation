#encoding: utf-8
#language: pt

@_login
Funcionalidade: Realizar Transfêrencia
  Eu como usuário do aplicativo Neon
  Quero acessar o menu de tranfêrencias
  Para realizar tranferencias entre contas DOC e TED

@regressao @transferencia
Cenário: Realizar transferência entre contas neon
  Dado que acesse o menu de transfêrencias
  Quando realizar uma transferência para o banco "neon"
  Então devo visualizar as informações do comprovante da transfência

@regressao @transferencia
Cenário: Realizar transfência para outro banco
  Dado que acesse o menu de transfêrencias
  Quando realizar uma transferência para o banco "bradesco"
  Então devo visualizar as informações do comprovante da transfência

@regressao @transferencia
Cenário: Cadastrar novo contato
  Dado que acesse o menu de transfêrencias
  Quando cadastrar um novo contato
  Então devo visualizar as informações do comprovante da transfência

@regressao @transferencia
Cenário: Realizar agendamento de transfência entre contas neon
  Dado que acesse o menu de transfêrencias
  Quando realizar um agendamento de transferência para o banco "neon"
  Então devo visualizar as informações do comprovante de agendamento

@regressao @transferencia
Cenário: Realizar agendamento de transfência entre contas de outros bancos
  Dado que acesse o menu de transfêrencias
  Quando realizar um agendamento de transferência para o banco "bradesco"
  Então devo visualizar as informações do comprovante de agendamento

@transferencia @regressao
Cenário: Realizar agendamento de transfência outros bancos dia não util
  Dado que acesse o menu de transfêrencias
  Quando realizar uma agendamento de transferencia para outro banco dia nao util "bradesco"
  Então devo visualizar as informações do comprovante dia util

@transferencia
Cenário: Realizar transferência para uma nova conta de um contato já existente
  Dado que acesse o menu de transfêrencias
  Quando cadastrar uma nova conta de um contato já existente
  Então devo visualizar as informações do comprovante da transfência
