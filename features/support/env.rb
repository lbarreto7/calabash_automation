require 'calabash-android/calabash_steps'
require 'calabash-android/cucumber'
require 'calabash-android/abase'
require 'calabash-android/operations'
require 'calabash-cucumber'
require "yaml"
require "pry"
require "pry-nav"
require "faker"
require "cpf_faker"
require "time"
require "boleto_bancario"
require 'httparty'
require 'tiny_tds'

ENV['ENV'] = 'DEV' unless ENV.key?'ENV'

## Massa de dados
MASSA = YAML.load_file('./fixtures/default_data.yml')[ENV['ENV']]
## Mensagens
MENSAGENS = YAML.load_file('./fixtures/messages.yml')[ENV['ENV']]
## Querys
QUERYS = YAML.load_file('./fixtures/querys.yml')[ENV['ENV']]
ONBOARDING = YAML.load_file('./fixtures/onboarding_txt.yml')[ENV['ENV']]
