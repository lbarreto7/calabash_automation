#encoding: utf-8
#language: pt

Funcionalidade: Cadastro
  Eu como futuro cliente da Neon
  Quero realizar o cadastro no APP
  Para ter acesso aos serviços oferecidos

@cadastro
Cenário: Abrir conta
  Dado que acesse a opção iniciar cadastro na tela inicial do App Neon
  Quando informar todos os dados solicitados
  Então devo aguardar a analise do meu cadastro para ser aprovado ou reprovado
