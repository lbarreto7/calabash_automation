#encoding: utf-8
#language: pt

@_login
Funcionalidade: Recarga de Celular
  Eu como usuário do aplicativo Neon
  Quero acessar o menu de recarga
  Para realizar recargas

@regressao @recarga
Cenário: Realizar recarga de celular
  Dado acesse o menu de recarga
  Quando realizar uma recarga
  Então devo visualizar o comprovante da recarga

@regressao @recarga
 Cenário: Excluir uma recarga avulsa
  Dado acesse o menu de recarga
  Quando realizar exclusão de uma recarga avulsa
  Então nao devo mais visualizar o numero na lista

@regressao @recarga
Cenário: Realizar recarga programda
  Dado acesse o menu de recarga
  Quando realizar uma recarga programda
  Então devo visualizar a recarga cadastrada

@recarga
Cenário: Excluir uma recarga programda
  Dado acesse o menu de recarga
  Quando realizar exclusão de uma recarga avulsa
  Então nao devo mais visualizar o numero na lista

@recarga
Cenário: Editar recarga recorrente
  Dado acesse o menu de recarga
  Quando selecionar recorrentes
  Então devo visualizar a recarga recorrentes
