#encoding: utf-8
#language: pt

Funcionalidade: Login
  Eu como usuário
  Quero acessar o aplicativo do Banco Neon
  Para visualizar as informações da minha conta

@regressao @login
Cenário: Realizar login
  Dado que acesse o aplicativo Neon
  Quando realizar o login com um "cliente_pf"
  Então devo visualizar a home do aplicativo
