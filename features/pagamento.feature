#encoding: utf-8
#language: pt

@_login
Funcionalidade: Realizar Pagamentos
  Eu como usuário do aplicativo Neon
  Quero acessar o menu de pagamentos
  Para realizar pagamentos de contas

@regressao @pagamento
Cenário: Realizar pagamento de boleto neon
  Dado que acesse o menu de pagamento para pagar um boleto neon
  Quando realizar o pagamento de boleto
  Então devo visualizar o comprovante do pagamento

@regressao @pagamento
Cenário: Realizar agendamento de pagamento de boleto neon
  Dado que acesse o menu de pagamento para pagar um boleto neon
  Quando realizar o agendamento do pagamento de boleto
  Então devo visualizar o comprovante do pagamento

@regressao @pagamento
Cenário: Realizar agendamento de pagamento de boleto de outro banco
  Dado que acesse o menu de pagamento
  Quando realizar o agendamento do pagamento de boleto outro banco
  Então devo visualizar o comprovante do pagamento

@regressao @pagamento
Cenário: Realizar pagamento de boleto de outro banco
  Dado que acesse o menu de pagamento
  Quando realizar pagamento de boleto outro banco
  Então devo visualizar o comprovante do pagamento
