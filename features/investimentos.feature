#encoding: utf-8
#language: pt
@_login @investimentos
Funcionalidade: Objetivos
  Eu como usuário
  Quero acessar o aplicativo do Banco Neon
  Para criar um objetivo ou alterar

@regressao
Cenário: Criar objetivo mensal
  Dado que acesso o menu de Investimentos
  Quando criar um novo objetivo "Total"
  Então devo visualizar o objetivo criado

@regressao
Cenário: Criar objetivo mensal
  Dado que acesso o menu de Investimentos
  Quando criar um novo objetivo "Excluir"
  Então devo visualizar o objetivo criado

@regressao
Cenário: Criar investimento livre
  Dado que acesso o menu de Investimentos
  Quando criar livre
  Então devo visualizar o investimento criado

@regressao
Cenário: Realizar deposito parcial
  Dado que acesso o menu de Investimentos
  Quando realizar um deposito parcial
  Então devo visualizar o valor parcial depositado no objetivo

@regressao
Cenário: Resgatar valor parcial
  Dado que acesso o menu de Investimentos
  Quando resgatar parcialmente um objetivo
  Então devo visualizar o objetivo resgatado

@regressao
Cenário: Realizar deposito total
  Dado que acesso o menu de Investimentos
  Quando realizar um deposito total
  Então devo visualizar o objetivo concluído

@regressao
Cenário: Resgatar valor total
  Dado que acesso o menu de Investimentos
  Quando resgatar o valor total do objetivo
  Então devo visualizar o objetivo resgatado total

@regressao
Cenário: Realizar depósito em livre
  Dado que acesso o menu de Investimentos
  Quando realizar um depósito igual ao valor mínimo
  Então devo visualizar que o valor foi creditado ao investimento livre

@regressao
Cenário: Realizar depósito menor que o mínimo
  Dado que acesso o menu de Investimentos
  Quando realizar um depósito menor que o valor mínimo
  Então devo visualizar uma mensagem informando qual o valor mínimo para aplicações em livre

@regressao
Cenário: Resgatar valor parcial em livre
  Dado que acesso o menu de Investimentos
  Quando realizar o resgate de um valor parcial do livre
  Então devo visualizar que o valor foi debitado do investimento livre

@regressao
Cenário: Resgatar valor total em livre
  Dado que acesso o menu de Investimentos
  Quando realizar o resgate total do livre
  Então devo visualizar que o valor foi debitado do investimento livre

@regressao
Cenário: Editar nome do investimento livre
  Dado que acesso o menu de Investimentos
  Quando editar o nome de um investimento livre
  Então devo visualizar o investimento com o nome alterado

@regressao
Cenário: Excluir investimento Objetivo
  Dado que acesso o menu de Investimentos
  Quando excluir um objetivo
  Então não devo visualizar o objetivo

@regressao
Cenário: Excluir investimento Livre
  Dado que acesso o menu de Investimentos
  Quando excluir um investimento livre
  Então não devo visualizar o livre
