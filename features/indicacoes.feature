#encoding: utf-8
#language: pt

@_login
Funcionalidade: Indicacoes
  Eu como usuario
  Quero acessar o aplicativo do Banco Neon
  Para visualizar as informacoes da aba indicacoes

@regressao @indicacoes
Cenário: Validar indicacoes
  Dado que acesso minhas indicacoes
  Quando passar pelo tutorial
  Então devo visualizar todas as indicacoes com sucesso

@regressao @indicacoes
Cenário: Validar o compartilhamento de convite
  Dado que acesso minhas indicacoes
  Quando passar pelo tutorial
  Então devo visualizar o botao compartilhar com sucesso

@regressao @indicacoes
Cenário: Validar detalhes de indicacoes
  Dado que acesso minhas indicacoes
  Quando acessar detalhes
  Então devo visualizar os status dos convites com sucesso

@regressao @indicacoes
Cenário: Validar as informacoes de termos de uso
  Dado que acesso minhas indicacoes
  Quando acessar detalhes
  Então devo visualizar o termo e as condicoes com sucesso
