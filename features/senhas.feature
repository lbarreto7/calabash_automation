#encoding: utf-8
#language: pt

Funcionalidade: Senhas
  Eu como usuário
  Quero acessar o aplicativo do Banco Neon
  Para alerar minhas senhas

@regressao @senha
Cenário: Alterar senha do cartão
  Dado que acesso a area do meu neon
  Quando alterar a senha do cartão
  Então devo visualizar a senha alterada
