#encoding: utf-8
#language: pt

@_login
Funcionalidade: Consultar Saldo
  Eu como usuário
  Quero acessar o saldo
  Para visualizar as informações de despesas da minha conta

@regressao @saldo
Cenário: Consultar saldo de despesas do mês
  Dado que realizar login
  Quando acessar o saldo total de despesas do mês
  Então devo visualizar as despesas do mês

@regressao @saldo
Cenário: Consultar saldo de despesas futuro
  Dado que realizar login
  Quando acessar o saldo de despesas futuro
  Então devo visualizar as despesas futuras da conta

@regressao @saldo
Cenário: Consultar saldo de despesas anteriores
  Dado que realizar login
  Quando acessar o saldo de despesas anteriores
  Então devo visualizar as despesas do mês

@regressao @saldo
Cenário: Consulta de pagamento agendado
  Dado que realizar login
  Quando realizo uma agendamento de pagamento
  Então devo visualizar os detalhes do agendamento

@regressao @saldo
Cenário: Exclusão de pagamento agendado
  Dado que acesse o saldo futuro
  Quando realizar a exclusão do agendamento
  Então não devo mais visualizar o agendamento da conta

@regressao @saldo
Cenário: Consultar detalhes de uma transação
  Dado que acesse o saldo do mês corrente
  Quando acessar o detalhe de uma transação
  Então devo visualizar os detalhes da transação

@regressao @saldo
Cenário: Exclusão de transferencia agendada
  Dado que acesse o saldo futuro
  Quando realizar a exclusão de uma transferencia agendada
  Então não devo mais visualizar o agendamento da conta

@regressao @saldo
Cenário: Consultar comprovante de agendamento de pagamento
  Dado que realizar login
  Quando acessar o saldo de despesas futuro
  Então visualizo comprovante de agendamento de pagamento
