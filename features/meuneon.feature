#encoding: utf-8
#language: pt
@_login
Funcionalidade: Meu Neon
  Eu como usuário do aplicativo Neon
  Quero acessar o menu meu neon
  Para solicitar ou alterar meus dados

@regressao @meu_neon
Cenário: Solicitar informe de rendimentos
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o informe de rendimentos com sucesso

@regressao @meu_neon
Cenário: Alterar / Novo e-mail
  Dado que acesso a area do meu neon
  Quando acessar email "cliente_pf"
  Então devo inserir o novo e-mail com sucesso

@regressao @meu_neon
Cenário: Acessar Ajuda via chat
  Dado que acesso a area do meu neon
  Quando acessar ajuda
  Então devo visualizar a faq com a perguntas frequentes

@regressao @meu_neon
Cenário: Acessar benefícios e tarifas
  Dado que acesso a area do meu neon
  Quando acessar benefícios e tarifas
  Então devo visualizar as tarifas e benefícos

@regressao @meu_neon
Cenário: Acessar sobre
  Dado que acesso a area do meu neon
  Quando acessar sobre
  Então devo visualizar as informações sobre o app

@regressao @meu_neon
Cenário: Realizar logout
  Dado que acesso a area do meu neon
  Quando realizar o logout
  Então devo visualizar o logout do app

@regressao @meu_neon
Cenário: Solicitar informe de tarifas
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o informe de tarifas com sucesso

@regressao @meu_neon
Cenário: Solicitar comprovante de titularidade
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o comprovante de titularidade

@regressao @meu_neon
Cenário: Solicitar extrato do crédito legado
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do crédito legado

@regressao @meu_neon
Cenário: Solicitar informe de redimentos
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar informe de rendimentos

@regressao @meu_neon
Cenário: Solicitar extrato do período dos útltimos 30 dias
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do periodo de "últimos 30 dias" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato do período dos útltimos 3 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do periodo de "últimos 3 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato do período dos útltimos 6 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do periodo de "últimos 6 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato do periodo dos últimos 9 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do periodo de "últimos 9 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato do periodo de 2018
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato do periodo de "2018" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos dos útltimos 30 dias
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato de investimento dos "últimos 30 dias" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos dos útltimos 3 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato de investimento dos "últimos 3 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos dos útltimos 6 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato de investimento dos "últimos 6 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos dos útltimos 9 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato de investimento dos "últimos 9 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos dos útltimos 12 meses
  Dado que acesso a area do meu neon
  Quando acessar solicitar documentos
  Então devo solicitar o extrato de investimento dos "últimos 12 meses" com sucesso

@regressao @meu_neon
Cenário: Solicitar extrato de investimentos um período específico
  Dado que acesso o extrato de investimento de um "período específico"
  Quando solicitar o extrato do ano "2019"
  Então devo solicitar o extrato do mês "janeiro" com sucesso
