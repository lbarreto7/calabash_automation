
  #encoding: utf-8

  #language: pt
  @_login @boleto
Funcionalidade: Gerar Boleto
  Eu como usuário do aplicativo Neon
  Quero acessar o menu de depósitos
  Para realizar a geração de boletos

@regressao @deposito
Cenário: Gerar boleto neon para pagamento
  Dado que acesse o menu de depósitos
  Quando realizar a geração de um boleto "normal"
  Então devo visualizar o boleto

@regressao @deposito
Cenário: Geral boleto neon com valor acima de oito mil
  Dado que acesse o menu de depósitos
  Quando realizar a geração de um boleto "NPC"
  Então devo vizualizar a mensagem "Aguarde a análise de segurança"
