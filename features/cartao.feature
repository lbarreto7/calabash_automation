#encoding: utf-8
#language: pt

@_login
Funcionalidade: Cartão
  Eu como usuário
  Quero acessar o aplicativo do Banco Neon
  Para alterar ou consultar dados dos meus cartões

@cartao @regressao 
Cenário: Consultar cartão virtual Débito
  Dado que acesso meus cartões
  Quando acessar o cartão virtual
  Então devo visualizar todas as informações do cartão

@cartao @regressao
Cenário: Consultar cartão virtual Crédito
  Dado que acesso meus cartões
  Quando acessar o cartão virtual de crédito
  Então devo visualizar todas as informações do cartão de crédito

@cartao @regressao
Cenário: Ativar bloqueio temporario do cartão virtual
  Dado que acesso meus cartões
  Quando ativar o bloqueio temporario do cartão virtual
  Então devo visualizar o cartão bloqueado temporariamente

@cartao @regressao
Cenário: Desativar bloqueio temporario do cartão virtual
  Dado que acesso meus cartões
  Quando desativar o bloqueio temporario do cartão virtual
  Então devo visualizar o cartão desbloqueado

@cartao @regressao
Cenário: Ativar bloqueio temporario do cartão físico
  Dado que acesso meus cartões
  Quando ativar o bloqueio temporario do cartão físico
  Então devo visualizar o cartão bloqueado temporariamente

@cartao @regressao
Cenário: Desativar bloqueio temporario do cartão físico
  Dado que acesso meus cartões
  Quando desativar o bloqueio temporario do cartão físico
  Então devo visualizar o cartão físico desbloqueado

@cartao @regressao @ativacao 
Cenário: Ativacao do cartao fisico
  Dado que acesse o meus cartoes com um usuario com cartao fisico no status entregue
  Quando solicitar a ativação do cartão fisico
  Então o cartao deve ser ativado com sucesso


