class Recarga < Calabash::ABase
  def acessar_recarga
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    page(Comum).validar_texto('recargas')
    touch("* text:'recargas'")
  end

  def escolher_celular
    page(Comum).validar_elemento('simple_button_txt_text')
    tap_when_element_exists("* id:'simple_button_txt_text'")
  end

  def botao_add_numero
    page(Comum).validar_texto('Qual número quer recarregar?')
    page(Comum).validar_elemento('menu_phone_recharge_contact_add')
    touch("* id:'menu_phone_recharge_contact_add'")
  end

  def informar_nome
    page(Comum).validar_texto('novo cadastro')
    page(Comum).validar_elemento('phone_recharge_add_contact_edt_name')
    touch("* id:'phone_recharge_add_contact_edt_name'")
    keyboard_enter_text('Automacao')
  end

  def informar_ddd
    page(Comum).validar_texto('novo cadastro')
    page(Comum).validar_elemento('phone_recharge_add_contact_edt_ddd')
    touch("* id:'phone_recharge_add_contact_edt_ddd'")
    page(Comum).validar_texto('escolher ddd')
    page(Comum).validar_texto('11')
    touch(query("* text:'OK'"))
  end

  def informar_numero
    page(Comum).validar_texto('novo cadastro', 'ADICIONAR CELULAR')
    page(Comum).validar_elemento('phone_recharge_add_contact_edt_number')
    touch("* id:'phone_recharge_add_contact_edt_number'")
    $numero_celular = Faker::Base.numerify('9########')
    keyboard_enter_text $numero_celular
    hide_soft_keyboard
  end

  def nao_add_agenda
    page(Comum).validar_texto('SEGUIR SEM ADICIONAR')
    touch("* text:'SEGUIR SEM ADICIONAR'")
  end

  def selecionar_operadora
    page(Comum).validar_texto('qual operadora?')
    page(Comum).validar_elemento('phone_recharge_txt_vendor')
    touch("* id:'phone_recharge_txt_vendor'")
    touch("* text:'OK'")
  end

  def selecionar_valor(quantity)
    page(Comum).validar_texto('qual valor?')
    page(Comum).validar_elemento('view_value_edt_value')
    touch("* id:'view_value_edt_value'")
      quantity.times do
      pan("* id:'dialog_default_ctn_content'", :up)
    end
    touch("* text:'OK'")
  end

  def repetir_recarga
    page(Comum).validar_elemento('phone_recharge_confirm_swt_schedule')
    touch("* id:'phone_recharge_confirm_swt_schedule'")
  end

  def recarga_recorrente
    page(Comum).validar_texto('AGENDAR')
    touch("* text:'AGENDAR'")
  end

  def selecionoar_numero_cadastrado(numero)
    page(Comum).scroll_ate_elemento_aparecer("* text:'#{numero}'")
    page(Comum).validar_texto('buscar', 'Qual número quer recarregar?')
    touch("* text:'#{numero}'")
  end

  def botao_excluir
    page(Comum).validar_elemento('menu_phone_recharge_confirm_remove')
    touch("* id:'menu_phone_recharge_confirm_remove'")
  end

  def confirmar_exclusao
    page(Comum).validar_texto('excluir', 'Tem certeza que quer excluir esse cadastro de recarga? As informações serão perdidas, mas você pode refazer a recarga quando quiser.')
    touch("* text:'EXCLUIR'")
  end

  def botao_repetir_recarga
    page(Comum).validar_texto('confirmar')
    page(Comum).validar_elemento('phone_recharge_confirm_swt_schedule')
    touch("* id:'phone_recharge_confirm_swt_schedule'")
  end

  def botao_agendar
    page(Comum).validar_texto('recarga recorrente', 'Próximas recargas')
    touch("* text:'AGENDAR'")
  end

  def selecionar_recorrentes
    page(Comum).validar_texto('recorrentes')
    touch("* text:'recorrentes'")
  end

  def remover_recorrentes
    page(Comum).validar_elemento('item_phone_recharge_history_txt_last_recharge')
    touch("* id:'item_phone_recharge_history_txt_last_recharge'")
  end
end
