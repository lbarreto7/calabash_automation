class Cartao < Calabash::ABase
  def acessar_cartao
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    page(Comum).validar_texto('cartões')
    page(Comum).validar_elemento('dashboard_txt_cards')
    tap_when_element_exists("* id:'dashboard_txt_cards'")
    page(Comum).swipe_direita('card_status_onboard_indicator', 5)
    touch("* id:'simple_button_txt_text'")
  end

  def acessar_menu_rastreio
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    page(Comum).validar_texto('cartões')
    page(Comum).validar_elemento('dashboard_txt_cards')
    tap_when_element_exists("* id:'dashboard_txt_cards'")
  end

  def acessar_cartao_virtual
    page(Comum).validar_texto('cartões', 'físico', 'virtual')
    tap_when_element_exists("* id:'card_status_txt_virtual'")
  end

  def ativar_desativar_bloqueio_virtual
    page(Comum).validar_texto('bloqueio temporário')
    page(Comum).validar_elemento('card_virtual_swt_lock')
    tap_when_element_exists("* id:'card_virtual_swt_lock'")
  end

  def ativar_desativar_bloqueio_fisico
    page(Comum).validar_texto('bloqueio temporário')
    page(Comum).validar_elemento('card_physical_swt_lock')
    tap_when_element_exists("* id:'card_physical_swt_lock'")
  end

  def botao_trocar_cartao
    page(Comum).validar_texto('Cobrado na Hora')
    page(Comum).validar_elemento('virtual_card_btn_switch')
    tap_when_element_exists("* id:'virtual_card_btn_switch'")
  end

  def informar_codigo(elemento, digitos)
    page(Comum).validar_elemento("#{elemento}")
    touch("* id:'#{elemento}'")
    keyboard_enter_text("#{digitos}")
    hide_soft_keyboard
  end
  
end
