class Transferencia < Calabash::ABase
  def acessar_transferencia
    page(Comum).validar_texto('transferências')
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    tap_when_element_exists("* text:'transferências'")
  end

  def fechar_facebook
    page(Comum).validar_texto('AGORA NÃO...')
    touch("* text:'AGORA NÃO...'")
  end

  def selecionar_favorecido(cliente)
    page(Comum).validar_elemento('list_contact_txt_name')
      wait_poll(until_exists: "* text:'#{cliente}'", timeout: 30) do
        pan("* id:'viewPager'",:up)
      end
    touch("* text:'#{cliente}'")
  end

  def selecionar_conta(banco)
    page(Comum).validar_elemento('imageDisclosure', 'avatar_plus_ctn_border')
      wait_poll(until_exists: "* text:'#{banco}'", timeout: 30) do
        pan("* id:'viewPager'",:up)
      end
    touch("* text:'#{banco}'")
  end

  def informar_valor
    @valor = Faker::Number.decimal(3)
    page(Comum).validar_elemento('tx_value')
    keyboard_enter_text(@valor)
  end

  def botao_proximo
    page(Comum).validar_elemento('btn_text')
    touch("* id:'btn_text'")
  end

  def scroll_up
    page(Comum).validar_elemento('transfer_proceed_txt_name')
    pan("* id:'transfer_holder'", :up, from: {x:0, y:0}, to: {x:-200, y:-200})
  end

  def btn_adicionar_contato
    page(Comum).validar_texto('todos', 'cadastrados', 'recorrentes')
    page(Comum).validar_elemento('action_add_contact')
    touch("* id:'action_add_contact'")
  end

  def preencher_cpf_cnpj
    $cpf = Faker::CPF.numeric
    page(Comum).validar_texto('Digite os números do CPF ou CNPJ')
    touch("* id:'tx_cpf_cnpj'")
    keyboard_enter_text($cpf)
  end

  def selecionar_banco(banco)
    page(Comum).validar_elemento('tx_banco_name')
    touch("* text:'#{banco}'")
  end

  def preencher_agencia(ag)
    page(Comum).validar_texto('qual a agência?')
    page(Comum).validar_elemento('tx_agencia_number')
    touch("* id:'tx_agencia_number'")
    keyboard_enter_text(ag)
  end

  def preencher_conta(conta)
    page(Comum).validar_texto('qual a conta?')
    page(Comum).validar_elemento('tx_cc_account_number')
    touch("* id:'tx_cc_account_number'")
    keyboard_enter_text(conta)
  end

  def informar_valor
    valor = Faker::Number.decimal(1)
    page(Comum).validar_texto('qual é o valor?')
    page(Comum).validar_elemento('transfer_value_txt_value')
    touch("* id:'transfer_value_txt_value'")
    keyboard_enter_text(valor)
  end

  def preencher_nome
    if element_exists("* text:'confirma o nome?'")
      page(Comum).validar_texto('confirma o nome?')
      touch("* id:'dialog_default_btn_positive'")
    else
      element_exists("* id:'contact_register_txt_name'")
      touch("* id:'contact_register_txt_name'")
      keyboard_enter_text('Automação android')
    end
  end

  def selecionar_data(ano, mes, dia)
    page(Comum).validar_elemento('tv_date')
    touch("* id:'tv_date'")
    page(Comum).validar_elemento('button1')
    set_date("datePicker", ano, mes, dia)
    touch("* id:'button1'")
  end

  def confirmar_nome
    page(Comum).validar_texto('confirma o nome?')
    page(Comum).validar_elemento('dialog_default_txt_message')
    touch("* id:'dialog_default_btn_positive'")
  end

  def botao_sim_agendar
    page(Comum).validar_texto('SIM, AGENDAR')
    touch("* text:'SIM, AGENDAR'")
  end

  def botao_sim_transferir
    page(Comum).validar_texto('SIM, TRANSFERIR')
    touch("* text:'SIM, TRANSFERIR'")
  end

  def btn_cadastrados
    page(Comum).validar_texto('cadastrados')
    page(Comum).validar_elemento('contact_tab_list_title_registered')
    tap_when_element_exists("* id:'contact_tab_list_title_registered'")
  end

  def cadastrar_novo_banco
    page(Comum).validar_elemento('list_contact_txt_name')
    tap_when_element_exists("* id:'list_contact_txt_name'")
    page(Comum).scroll_ate_elemento_aparecer("* text:'Adicionar nova conta'")
    page(Comum).validar_texto('Adicionar nova conta')
    tap_when_element_exists("* text:'Adicionar nova conta'")
  end

  def validar_data_transferencia
    page(Comum).validar_texto('transferir quando?')
    page(Comum).validar_texto('Transferir hoje')
    page(Comum).botao_proximo
  end

end
