class Login < Calabash::ABase
  def on_boarding_login
    page(Comum).validar_texto(ONBOARDING['login']['titulo_1'])
    page(Comum).validar_texto(ONBOARDING['login']['mensagem_1'])
    page(Comum).swipe_direita('new_home_onboard_view_pager', 1)
    page(Comum).validar_texto(ONBOARDING['login']['mensagem_2'])
    page(Comum).swipe_direita('new_home_onboard_view_pager', 1)
    page(Comum).validar_texto(ONBOARDING['login']['titulo_2'])
    page(Comum).validar_texto(ONBOARDING['login']['mensagem_3'])
    page(Comum).swipe_direita('new_home_onboard_view_pager', 1)
    page(Comum).validar_texto(ONBOARDING['login']['titulo_3'])
    page(Comum).validar_texto(ONBOARDING['login']['mensagem_4'])
    page(Comum).validar_texto('CADASTRAR', 'ENTRAR')
  end

  def botao_entrar
    page(Comum).validar_texto('ENTRAR')
    touch("* text:'ENTRAR'")
  end

  def botao_continuar
    page(Comum).validar_texto('ENTRAR NO APP')
    touch("* text:'ENTRAR NO APP'")
  end

  def digitar_email(email)
    page(Comum).validar_elemento('login_email_edt', 'box_button_txt_description')
    tap_when_element_exists("* id:'login_email_edt'")
    keyboard_enter_text("#{email}")
    hide_soft_keyboard
  end

  def digitar_senha(quantity)
    page(Comum).validar_texto('senha numérica', 'Digite sua senha:', 'ajuda', 'Digite sua senha:')
    quantity.times do
    tap_when_element_exists("* text:'1'")
    end
  end
end
