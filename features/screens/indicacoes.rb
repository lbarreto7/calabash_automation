class Indicacoes < Calabash::ABase
 def acessar_indicacoes
    page(Comum).scroll_ate_elemento_aparecer("* text:'indicações'")
    page(Comum).validar_texto('indicações')
    page(Comum).validar_elemento('dashboard_txt_referral')
    tap_when_element_exists("* text:'indicações'")
  end

 def btn_detalhes
    page(Comum).validar_elemento('action_info')
    tap_when_element_exists("* id:'action_info'")
  end

  def btn_condicoes_uso
    page(Comum).validar_texto('condições de uso')
    page(Comum).validar_elemento('referral_benefits_txt_terms_title')
    tap_when_element_exists("* id:'referral_benefits_txt_terms_title''")
  end
end
