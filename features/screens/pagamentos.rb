class Pagamento < Calabash::ABase
  def acessar_pagamentos
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    page(Comum).validar_texto('pagamentos')
    tap_when_element_exists("* text:'pagamentos'")
  end

  def acessar_codigo_de_barras
    page(Comum).validar_texto('pagamentos', 'Digitar código de barras')
    tap_when_element_exists("* text:'Digitar código de barras'")
  end

  def digitar_codigo_barras(codigo)
    page(Comum).validar_texto('pagamentos', 'Digite o código de barras do seu boleto.')
    tap_when_element_exists("* id:'payment_barcode_manual_edt_code'")
    keyboard_enter_text(codigo)
    tap_when_element_exists("* text:'pagamentos'")
    page(Comum).validar_texto('CONTINUAR')
    wait_for_keyboard
    hide_soft_keyboard
  end

  def botao_finalizar
    page(Comum).validar_elemento('simple_button_txt_text')
    tap_when_element_exists("* id:'simple_button_txt_text'")
  end

  def botao_ok
    page(Comum).validar_elemento('dialog_default_btn_positive')
    tap_when_element_exists("* id:'dialog_default_btn_positive'")
  end

  def botao_agendar
    page(Comum).validar_elemento('dialog_default_btn_positive')
    tap_when_element_exists("* id:'dialog_default_btn_positive'")
  end

  def acessar_data
    page(Comum).validar_elemento('payment_data_txt_date')
    tap_when_element_exists("* id:'payment_data_txt_date'")
  end

  def selecionar_dia
    page(Comum).validar_texto('Hoje', 'Data de pagamento', 'dados do pagamento', 'Vence em')
    tap_when_element_exists("* id:'payment_data_txt_date'")
  end
end
