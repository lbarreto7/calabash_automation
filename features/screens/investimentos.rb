class Investimentos < Calabash::ABase
  def acessar_investimentos
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    page(Comum).validar_texto('investimentos')
    page(Comum).validar_elemento('dashboard_txt_goals')
    touch(query("* id:'dashboard_txt_goals'"))
  end

  def escolher_investimento(texto)
    page(Comum).validar_elemento('toolbar_title')
    page(Comum).validar_elemento('investment_card_txt_sub_title')
    page(Comum).validar_elemento('investment_card_txt_description')
    page(Comum).validar_texto("#{texto}")
    touch(query("* text:'#{texto}'"))
  end

  def botao_criar_objetivo
    page(Comum).validar_elemento('toolbar_title', 'balance_txt_label', 'item_investment_creation_button')
    page(Comum).validar_texto('investimentos', 'disponíveis para investir', 'Criar novo Investimento')
    tap_when_element_exists("* id:'item_investment_creation_button'")
  end

  def informar_valor(valor)
    page(Comum).validar_elemento('view_value_edt_value')
    touch("* id:'view_value_edt_value'")
    clear_text_in("* id:'view_value_edt_value'")
    keyboard_enter_text valor
    page(Comum).validar_elemento('view_value_edt_value')
  end

  def informar_nome(name)
    page(Comum).validar_elemento('create_goal_name_ok', 'create_goal_name_edt_value')
    tap_when_element_exists("* id:'create_goal_name_edt_value'")
    keyboard_enter_text name
  end

  def selecionar_imagem_neon
    page(Comum).validar_elemento('rl_select_image')
    touch("* id:'rl_select_image'")
    page(Comum).validar_texto('Fotos Neon')
    touch("* text:'Fotos Neon'")
    page(Comum).validar_elemento('img_icon')
    touch("* id:'img_icon'")
    page(Comum).validar_texto('imagem')
    page(Comum).validar_texto('A foto do seu objetivo ficou ótima!')
    page(Comum).validar_elemento('create_goal_image_title')
    page(Comum).validar_elemento('create_goal_image_img')
    page(Comum).validar_texto('PRÓXIMO')
    flash("* id:'create_goal_image_img'")
  end

  def final_straight_screen
    page(Comum).aguarda_elemento_aparecer("* text:'reta final'")
    page(Comum).aguarda_elemento_aparecer("* text:'Esta é a data prevista para alcançar\nseu objetivo. Quer mudar?'")
    page(Comum).aguarda_elemento_aparecer("* text:'Data final'")
    page(Comum).aguarda_elemento_aparecer("* text:'Sem data final'")
    page(Comum).botao_proximo
  end

  def botao_confirmar
    page(Comum).validar_elemento('btn_text')
    touch("* id:'btn_text'")
  end

  def acessar_detalhe_obj(name)
    page(Comum).validar_elemento('toolbar_title', 'item_investment_card_root', 'balance_txt_label')
    page(Comum).scroll_ate_elemento_aparecer("#{name}")
    touch(query("#{name}"))
  end

  def acessar_deposito_resgatar
    page(Comum).validar_elemento('toolbar_title', 'simple_button_txt_text')
    page(Comum).botao_entendi
  end

  def acessar_deposito_investir
    page(Comum).validar_texto('INVESTIR')
    tap_when_element_exists("* text:'INVESTIR'")
  end

  def informar_valor_investimento(valor)
    page(Comum).validar_texto('disponíveis para investir')
    page(Comum).aguarda_elemento_aparecer("* text:'Quanto dinheiro você\nquer investir?'")
    page(Comum).validar_texto('Quero investir')
    clear_text_in("* id:'view_value_edt_value'")
    keyboard_enter_text valor
  end

  def informar_valor_resgate(valor)
    page(Comum).validar_texto('resgatar')
    page(Comum).validar_texto('Valor total')
    page(Comum).retornar_texto_do_elemento('investment_withdraw_txt_total_value')
    page(Comum).validar_texto('Desconto IR')
    page(Comum).retornar_texto_do_elemento('investment_withdraw_txt_ir_value')
    page(Comum).validar_texto('Desconto IOF')
    page(Comum).retornar_texto_do_elemento('investment_withdraw_txt_iof_value')
    page(Comum).validar_texto('Valor líquido')
    page(Comum).retornar_texto_do_elemento('investment_withdraw_txt_net_value')
    page(Comum).validar_texto('Quero resgatar')
    tap_when_element_exists("* id:'view_value_edt_value'")
    keyboard_enter_text valor
  end

  def botao_continuar
    page(Comum).validar_texto('CONTINUAR')
    touch("* text:'CONTINUAR'")
  end

  def botao_investir
    page(Comum).validar_texto('INVESTIR')
    tap_when_element_exists("* text:'INVESTIR'")
  end

  def realizar_deposito
    page(Comum).validar_elemento('investment_confirm_deposit_img_coin')
    pan("* id:'investment_confirm_deposit_img_coin'", :down, from: {x: 0, y: 0}, to: {x: 200, y:500})
  end

  def acessar_resgate
    page(Comum).validar_elemento('goal_deposit_rescue_txt_rescue')
    touch("* id:'goal_deposit_rescue_txt_rescue'")
  end

  def quebrar_porquinho(quantity)
      quantity.times do
        page(Comum).validar_elemento('investment_confirm_withdraw_lottie_pig')
        page(Comum).validar_texto('confirmar resgate')
        page(Comum).validar_texto('Valor solicitado')
        page(Comum).validar_texto('Desconto IR')
        page(Comum).validar_texto('Desconto IOF')
        page(Comum).validar_texto('Valor do resgate')
        tap_when_element_exists("* id:'investment_confirm_withdraw_lottie_pig'")
        page(Comum).validar_texto('O valor do resgate será retornado automaticamente para a sua conta.')
    end
  end

  def acionar_editar
    page(Comum).validar_elemento('goal_detail_img_edit')
    touch("* id:'goal_detail_img_edit'")
  end

  def alterar_texto
    $nome = Faker::Name.first_name
    clear_text_in("* id:'investment_edit_edt_name'")
    touch("* id:'investment_edit_edt_name'")
    keyboard_enter_text $nome
    wait_for_keyboard
    hide_soft_keyboard
  end

  def acionar_pronto
    hide_soft_keyboard
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def btn_valor_total
    page(Comum).validar_texto("VALOR TOTAL")
    tap_when_element_exists("* text:'VALOR TOTAL'")
  end

  def btn_editar
    tap_when_element_exists("* id:'investment_detail_action_more'")
    page(Comum).validar_elemento('edition_bottom_sheet_ctn_edit')
    page(Comum).validar_texto("Editar")
    tap_when_element_exists("* text:'Editar'")
  end

  def btn_excluir
    tap_when_element_exists("* id:'investment_detail_action_more'")
    page(Comum).validar_elemento('edition_bottom_sheet_ctn_delete')
    page(Comum).validar_texto('Excluir')
    tap_when_element_exists("* id:'edition_bottom_sheet_ctn_delete'")
  end

  def btn_sim_excluir
    page(Comum).validar_texto("SIM, EXCLUIR")
    tap_when_element_exists("* text:'SIM, EXCLUIR'")
  end

  def show_flow_objetivo_concluido
    page(Comum).validar_texto('Parabéns! Você alcançou seu objetivo!')
    page(Comum).validar_texto('Seu investimento continuará rendendo normalmente até que você decida resgatar.')
    tap_when_element_exists("* id:'engage_btn_confirm'")
  end

end
