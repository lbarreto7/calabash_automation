class Meu_neon < Calabash::ABase
  def acessar_meu_neon
    page(Comum).validar_texto('meu neon', 'saldo', 'crédito', 'investimentos')
    page(Comum).validar_elemento('dashboard_txt_my_neon')
    tap_when_element_exists("* id:'dashboard_txt_my_neon'")
  end


  def acessar_item_menu(texto)
    page(Comum).validar_texto("#{texto}")
    tap_when_element_exists("* text:'#{texto}'")
  end

  def acessar_periodo(periodo)
    page(Comum).validar_texto(periodo)
    tap_when_element_exists("* text:'#{periodo}'")
  end

  def acessar_senha_cartao
    page(Comum).validar_texto('senha do cartão')
    tap_when_element_exists("* text:'senha do cartão'")
  end

  def enviar_email
    page(Comum).validar_texto('SIM, ENVIAR')
    tap_when_element_exists("* text:'SIM, ENVIAR'")
  end

  def validar_email_enviado
    @msg = page(Comum).retornar_texto_do_elemento('neon_dialog_txt_message')
    puts "Enviado para #{@msg}"
  end

  def digitar_email(email)
    page(Comum).validar_elemento('change_email_txt_email')
    tap_when_element_exists("* id:'change_email_txt_email'")
    keyboard_enter_text("#{email}")
    hide_soft_keyboard
    page(Comum).validar_elemento('change_email_txt_confirm_email')
    tap_when_element_exists("* id:'change_email_txt_confirm_email'")
    keyboard_enter_text("#{email}")
    hide_soft_keyboard
  end

  def novo_email
    page(Comum).validar_texto('e-mail')
    tap_when_element_exists("* text:'e-mail'")
 end

 def confirmar_email
    page(Comum).validar_elemento('simple_button_txt_text')
    page(Comum).validar_texto('CONFIRMAR')
    tap_when_element_exists("* id:'simple_button_txt_text'")
 end

 def sair_app
   page(Comum).validar_texto('SIM', 'NÃO', 'Deseja sair do app?')
   tap_when_element_exists("* text:'SIM'")
 end
end
