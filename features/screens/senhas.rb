class Senha < Calabash::ABase
  def btn_criar_senha
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def digitar_nova_senha_cartao(num_1, num_2, num_3, num_4)
    page(Comum).validar_texto('0')
      touch("* text:'#{num_1}'")
      touch("* text:'#{num_2}'")
      touch("* text:'#{num_3}'")
      touch("* text:'#{num_4}'")
  end

  def preencher_nome_mae(nome)
    page(Comum).validar_elemento('validation_info_edt_name')
    touch("* id:'validation_info_edt_name'")
    keyboard_enter_text(nome)
  end

  def preencher_cel(nome)
    page(Comum).validar_elemento('validation_info_edt_phone')
    touch("* id:'validation_info_edt_phone'")
    keyboard_enter_text(11988229286)
  end
end
