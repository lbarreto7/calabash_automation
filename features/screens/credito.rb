class Credito < Calabash::ABase
#Onboarding de credito
  def acessar
    page(Comum).validar_elemento('dashboard_txt_credit')
    touch("* id:'dashboard_txt_credit'")
  end

  def swipe_esquerda(quantity)
    quantity.times do
    pan("* id:'content'", :left)
    end
  end

  def onboarding_botao
    page(Comum).validar_texto('QUE LEGAL!')
    touch("* id:'simple_button_txt_text'")
  end

  def botao_comecar
    page(Comum).validar_texto('COMEÇAR')
    touch("* id:'simple_button_txt_text'")
  end

#Pagamento de fatura total gerando com o boleto e debito em conta
  def botao_pagar_fatura
    until_element_exists("* id:'simple_button_txt_text'", timeout:1)
    touch("* id:'simple_button_txt_text'")
  end

  def botao_proximo
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def botao_pagar_parcela
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def botao_gerar_boleto
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* text:'GERAR BOLETO'")
  end

  def botao_gerar_boleto_mesmo_assim
    page(Comum).validar_elemento('dialog_default_btn_positive')
    touch("* id:'dialog_default_btn_positive'")
  end

  def botao_copiar_numero
    page(Comum).validar_elemento('credit_billet_ctn_bar_code')
    touch("* id:'credit_billet_ctn_bar_code'")
  end

  def botao_acessar_link
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def botao_debito_em_conta
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* text:'PAGAR COM A CONTA NEON'")
  end

  def botao_popup_confirmar
    page(Comum).validar_elemento('dialog_default_btn_positive')
    touch("* id:'dialog_default_btn_positive'")
  end

  def digitar_senha_cartao(quantity)
    quantity.times do
      touch("* text:'1'")
    end
  end

  def liquido_pagamento_fatura
    page(Comum).validar_elemento('credit_invoice_list_txt_closed_value')
    wait_for_elements_do_not_exist('wait_for_elements_do_not_exist')
  end

#Pagamento adiantado com o parcial com o boleto
  def botao_adiantar_pagamento
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* text:'ADIANTAR PAGAMENTO'")
  end

  def adiantamento_informar_valor_parcial
    @valor = "10,00"
    page(Comum).validar_elemento('credit_payment_edit_value')
    touch("* id:'credit_payment_edit_value'")
    clear_text_in("* id:'credit_payment_edit_value'")
    keyboard_enter_text(@valor)
    hide_soft_keyboard
  end

  def adiantamento_botao_pagar_agora
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* id:'simple_button_txt_text'")
  end

  def adiantamento_popup
    page(Comum).validar_elemento('dialog_default_btn_positive')
    touch("* text:'SIM'")
  end

  def adiantamento_botao_gerar_boleto
    page(Comum).validar_elemento('simple_button_txt_text')
    touch("* text:'GERAR BOLETO'")
  end

#Consultar faturas futuras e atual
  def faturas_futuras
    until_element_exists("* id:'credit_invoice_list_txt_future_label'", timeout:1)
    touch("* id:'credit_invoice_list_txt_future_label'")
  end

  def fatura_atual
    until_element_exists("* id:'credit_invoice_list_txt_future_label'", timeout:1)
    touch("* id:'credit_invoice_list_txt_current_label'")
  end

  def resumo_de_credito
    until_element_exists("* id:'credit_resume_ctn_fragment'")
  end
end
