class Saldo < Calabash::ABase
  def acessar_saldo
    page(Comum).scroll_ate_elemento_aparecer("* text:'recargas'")
    page(Comum).validar_texto('depósitos', 'pagamentos', 'transferências', 'cartões')
    page(Comum).scroll_ate_elemento_aparecer_up("* text:'meu neon'")
    page(Comum).validar_texto('meu neon', 'saldo', 'crédito', 'investimentos', 'cartões')
    tap_when_element_exists("* text:'saldo'")
  end

  def acessar_saldo_futuros
    page(Comum).validar_texto('Futuros', 'Total de despesas')
    page(Comum).validar_elemento('action_search', 'balance_ctn_categories', 'iv_icon', 'tv_category')
    touch("* text:'Futuros'")
  end

  def acessar_saldo_enteriores
    page(Comum).validar_texto('Futuros', 'Total de despesas')
    page(Comum).validar_elemento('tv_value', 'balance_txt_value', 'action_search', 'balance_ctn_categories', 'tv_category', 'iv_icon')
    pan("* id:'balance_ctn_months'", :right, from: {x: 0, y: 0}, to: {x: 50, y:150})
  end

  def acessar_item(texto)
    page(Comum).aguardar_elemento_sumir("* text:'carregando dados...'")
    page(Comum).aguardar_elemento_sumir("* id:'balance_lottie_loading'")
    page(Comum).validar_elemento('tv_category', 'tv_value', 'tv_description', 'tv_date', 'iv_icon')
    page(Comum).scroll_ate_elemento_aparecer("* {text CONTAINS '#{texto}'}")
    touch(query("* {text CONTAINS '#{texto}'}"))
  end

  def acessar_comprovante
    touch("*marked:'OPEN'", :offset => {:x => 0, :y => 980})
    if (query("* id:'receipt_iv_logo'") != [])
    touch("* id:'receipt_iv_logo'")
    end
  end

  def excluir_item
    page(Comum).validar_elemento('transaction_detail_txt_name', 'transaction_detail_btn_delete')
    touch("* id:'transaction_detail_btn_delete'")
    page(Comum).validar_texto('SIM, EXCLUIR', 'NÃO')
    touch("* text:'SIM, EXCLUIR'")
  end
end
