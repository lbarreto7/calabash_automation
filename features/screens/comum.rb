class Comum < Calabash::ABase
  def digitar_senha_cartao(quantity)
    page(Comum).validar_texto('senha do cartão')
    quantity.times do
      touch("* text:'1'")
    end
  end

  def swipe_direita(elemento, quantity)
    page(Comum).validar_elemento("#{elemento}")
    quantity.times do
      pan("* id:'#{elemento}'",:left)
    end
  end

  def swipe_desquerda(elemento, quantity)
    page(Comum).validar_elemento('onboard_indicator')
    quantity.times do
      pan("* id:'#{elemento}'",:right)
    end
  end

  def botao_proximo
    page(Comum).validar_elemento('simple_button_txt_text')
    tap_when_element_exists("* id:'simple_button_txt_text'")
  end

  def botao_proximo_old
    page(Comum).validar_elemento('toolbar_title', 'toolbar')
    page(Comum).validar_elemento('btn_text')
    tap_when_element_exists("* id:'btn_text'")
  end

  def botao_sim
    page(Comum).validar_texto('SIM')
    tap_when_element_exists("* text:'SIM'")
  end

  def botao_entendi
    page(Comum).validar_elemento('simple_button_txt_text')
    tap_when_element_exists("* id:'simple_button_txt_text'")
  end

  def scroll_para_baixo(quantity)
    quantity.times do
      scroll('ScrollView', :down)
    end
  end

  def aguarda_elemento_aparecer(elemento)
    wait_poll(until_exists: elemento.to_s, timeout: 60)
  end

  def aguardar_elemento_sumir(consulta)
    wait_for_element_does_not_exist("#{consulta}", timeout: 60)
  end

  def validar_texto(*args)
    args.each do |text|
      wait_for_elements_exist(["* text:'#{text}'"], timeout: 90)
    end
  end

  def validar_texto_com_quebra(text)
    wait_for_elements_exist(["* text:'#{text}'"], timeout: 90)
  end

  def validar_elemento(*args)
    args.each do |elements|
      wait_for_elements_exist(["* id:'#{elements}'"], timeout: 90)
    end
  end

  def contem_texto(texto)
    wait_for_elements_exist(["* {text CONTAINS '#{texto}'}"], timeout: 90)
  end

  def scroll_down(quantity)
    quantity.times do
      perform_action('drag', 90, 0, 90, 50, 50)
    end
  end

  def scroll_ate_elemento_aparecer(consulta)
    wait_poll(until_exists: consulta.to_s, timeout: 60) do
      scroll_down(2)
    end
  end

  def scroll_ate_elemento_aparecer_up(consulta)
    wait_poll(until_exists: consulta.to_s, timeout: 60) do
      perform_action('drag', 90, 0, 70, 100, 20)
    end
  end

  def retornar_texto_do_elemento(consulta, indice = 0)
    page(Comum).validar_elemento(consulta)
    valor = query("* id:'#{consulta}' index:#{indice}", :text).first
      if (valor.nil? || valor.kind_of?(String) == false || valor.empty?)
         "Texto nulo ou vazio"
      end
    return valor
  end

  def selecionar_categoria
    page(Comum).validar_elemento('payment_desc_cat_txt', 'payment_desc_cat_edt_desc')
    page(Comum).validar_texto('SELECIONE A CATEGORIA')
    tap_when_element_exists("* id:'payment_desc_cat_txt'")
    page(Comum).scroll_ate_elemento_aparecer("* text:'CELULAR'")
    touch("* text:'CELULAR'")
  end

  def selecionar_data(ano, mes, dia)
    page(Comum).validar_elemento('button1')
    set_date("datePicker", ano, mes, dia)
    tap_when_element_exists("* id:'button1'")
  end

  # def selecionar_ambiente(params, dev = 7)
  #   page(Login).on_boarding_login
  #   page(Login).botao_entrar
  #   page(Comum).validar_elemento('action_develop_config', 'simple_button_txt_text')
  #   tap_when_element_exists("* id:'action_develop_config'")
  #   page(Comum).scroll_ate_elemento_aparecer_up("* text:'Enviroment'")
  #   page(Comum).validar_texto('Url', 'Enviroment', 'CONFIGURATION', 'Force validated device')
  #   touch(query("* id:'text1'"))
  #   page(Comum).validar_texto("#{params}")
  #   touch(query("* text:'#{params}'"))
  #   page(Comum).scroll_ate_elemento_aparecer("* id:'simple_button_txt_text'")
  #   tap_when_element_exists("* id:'simple_button_txt_text'")
  # end

  def aguardar_animacao_de_elemento(id_elemento)
    cord_y = 0
    begin
      aux = cord_y
      cord_y = query("* id:'#{id_elemento}'").first
      sleep 0.3
    end while aux =! cord_y
  end
end
