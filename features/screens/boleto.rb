class Deposito < Calabash::ABase
  def acessar_deposito
    page(Comum).scroll_ate_elemento_aparecer("* text:'depósitos'")
    tap_when_element_exists("* text:'depósitos'")
  end

  def btn_deposito
    page(Comum).validar_texto('Depósito por boleto', 'Dinheiro disponível em até 2 dias úteis.', 'Transferência bancária')
    tap_when_element_exists("* text:'Depósito por boleto'")
  end

  def informar_valor_boleto(boleto)
    page(Comum).validar_texto('Valor do boleto')
    tap_when_element_exists("* id:'view_value_edt_value'")
    keyboard_enter_text("#{boleto}")
    hide_soft_keyboard
  end
end
