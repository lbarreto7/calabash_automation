require 'uri'
require 'net/http'
require 'json'
class Cadastro < Calabash::ABase
  def btn_abrir_conta
    page(Comum).validar_elemento('simple_button_txt_text')
    page(Comum).validar_texto('CADASTRAR')
    touch("* text:'CADASTRAR'")
  end

  def btn_config
    touch("* id:'action_develop_config'")
    touch("* id:'develop_settings_btn_features'")
  end

  def config_amb
    tap_when_element_exists("* text:'IGNORE_DAON_ANALYSIS_QUALITY'")
    25.times do flick("* text:'CANCEL_CHARGE_BACK'",:up) end
    page(Comum).validar_texto('NATIVE_CAMERA', 'FEATURES')
    tap_when_element_exists("* text:'NATIVE_CAMERA'")
    tap_when_element_exists("* id:'simple_button_txt_text'")
    page(Comum).validar_texto('SALVAR', 'Mocks', 'Force validated device', 'Features', 'Enviroment', 'Flows')
    page(Comum).validar_elemento('develop_settings_btn_save', 'neon_list_item_txt_subtitle', 'simple_button_bg')
    page(Cadastro).click_button("* id:'develop_settings_btn_save'", 2)
  end

  def ck_termos_aceite
    page(Comum).validar_elemento('register_welcome_cbx_terms')
    tap_when_element_exists("* id:'register_welcome_cbx_terms'")
  end

  def click_button(element, quantity)
    quantity.times do
      tap_when_element_exists("#{element}")
    end
  end

  def btn_iniciar_cadastro
    page(Comum).validar_texto('INICIAR CADASTRO')
    touch("* text:'INICIAR CADASTRO'")
  end

  def informa_CPF(doc)
    page(Comum).validar_texto('E qual é seu CPF? Não precisa colocar pontos ou traços.')
    keyboard_enter_text(("#{doc}"))
    puts "CPF: #{doc}"
    page(Comum).validar_texto('VOLTAR')
    page(Comum).validar_elemento('simple_button_txt_text')
    hide_soft_keyboard
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
    touch("* text:'PRÓXIMO'")
  end

  def txt_nome_completo
    page(Comum).validar_texto('Seu sexo')
    page(Comum).aguarda_elemento_aparecer("* id:'simple_button_txt_text'")
    touch("* id:'register_name_input_name'")
    keyboard_enter_text('Bruna Silva')
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    page(Comum).validar_texto('Seu sexo')
    page(Comum).aguarda_elemento_aparecer("* id:'register_name_radio_female'")
    touch("* id:'register_name_radio_female'")
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def data_nasc
    page(Comum).validar_elemento('register_birthday_input_birthday')
    touch("* id:'register_birthday_input_birthday'")
    page(Comum).validar_texto('CONFIRMAR')
    touch("* id:'neon_date_picker_btn_date'")
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def email_cadastro
    @email = Faker::Internet.email
    page(Comum).validar_texto('Qual é o seu e-mail? Prometemos que não vamos enviar spam.')
    touch("*id:'register_email_input_email'")
    keyboard_enter_text(@email)
    touch("* id:'register_email_input_confirm'")
    keyboard_enter_text(@email)
    page(Comum).validar_texto('PRÓXIMO')
    hide_soft_keyboard
    page(Comum).validar_texto('VOLTAR')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def txt_nome_mae
    page(Comum).validar_texto('Agora, precisamos que você preencha o nome completo da sua mãe.')
    keyboard_enter_text('Mae QA')
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def edt_cep
    page(Comum).validar_texto('Boa! Agora vamos para seu endereço residencial. Conta pra gente onde você mora.')
    page(Comum).validar_elemento('zipcode_edt_zipcode')
    keyboard_enter_text('04313150')
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
    page(Cadastro).edt_numero
  end

  def edt_numero
    page(Comum).validar_elemento('address_edt_number')
    page(Comum).validar_texto('Boa! Agora vamos para seu endereço residencial. Conta pra gente onde você mora.', 'VOLTAR')
    touch("* id:'address_edt_number'")
    keyboard_enter_text(5)
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    tap_when_element_exists("* text:'PRÓXIMO'")
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def edt_senha
    page(Comum).validar_texto('1', '2', '3')
    page(Comum).validar_elemento('neon_password_ctn_circles')
    touch("* text:'1'")
    touch("* text:'2'")
    touch("* text:'3'")
    touch("* text:'6'")
    touch("* text:'5'")
    touch("* text:'4'")
  end

  def btn_entendi
    page(Comum).validar_elemento('simple_button_txt_text')
    page(Comum).scroll_ate_elemento_aparecer("* text:'ENTENDI'")
    tap_when_element_exists("* text:'ENTENDI'")
  end

  def confirm_senha
    page(Comum).validar_texto('confirme sua senha', 'Boa! Agora digite sua senha de novo para confirmar.')
    page(Cadastro).btn_entendi
    page(Cadastro).edt_senha
    page(Cadastro).senha_criada
    tap_when_element_exists("* text:'PRÓXIMO'")
  end

  def senha_criada
    page(Comum).validar_texto('senha criada')
    page(Comum).validar_texto('PRÓXIMO')
  end

  def nm_celular
    page(Comum).validar_texto("Qual é o número do seu celular?")
    page(Comum).validar_elemento("register_phone_input_phone")
    touch("* id:'register_phone_input_phone'")
    @phone = "11" + "9" + Faker::Number.number(8)
    keyboard_enter_text(@phone)
    puts "phone: #{@phone}"
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def input_token(token)
    page(Comum).validar_elemento('register_token_input_token')
    page(Comum).contem_texto('Digite o código de verificação que chegou no número')
    page(Cadastro).click_button("* id:'register_token_input_token'", 2)
    keyboard_enter_text(token)
    hide_soft_keyboard
    page(Comum).validar_texto('PRÓXIMO')
    touch("* text:'PRÓXIMO'")
    page(Comum).validar_texto('celular confirmado')
    page(Comum).validar_texto('Seu celular foi confirmado com sucesso.')
    page(Comum).validar_texto('BELEZA')
    page(Cadastro).click_button("* text:'BELEZA'", 1)
  end

  def resp_questoes_selfie
    page(Comum).validar_texto('Posicione o celular na altura dos olhos.', 'Tire a foto em um lugar bem iluminado.', 'Retire qualquer acessório.', 'Não sorria ou faça caretas.')
    page(Cadastro).click_button("* text:'LI E ENTENDI'", 2)
    page(Comum).validar_texto('1. Seu rosto está visível, centralizado e focado?')
    page(Cadastro).click_button("* id:'register_selfie_question_btn_yes'", 3)
    page(Comum).validar_texto('2. A foto está bem iluminada?')
    page(Cadastro).click_button("* id:'register_selfie_question_btn_yes'", 3)
    page(Comum).validar_texto('3. Você está sem chapéu, óculos ou acessórios?')
    page(Cadastro).click_button("* id:'register_selfie_question_btn_yes'", 3)
    page(Comum).validar_texto('4. Sua expressão está relaxada, sem sorrisos ou caretas?')
    page(Cadastro).click_button("* id:'register_selfie_question_btn_yes'", 3)
  end

  def nm_renda
    page(Comum).validar_texto('Qual é sua renda mensal? Precisamos saber para fazer uma análise melhor.')
    page(Comum).aguarda_elemento_aparecer("* id:'view_value_txt_label'")
    touch("* id:'view_value_txt_label'")
    keyboard_enter_text(1500)
    hide_soft_keyboard
    page(Comum).aguarda_elemento_aparecer("* id:'view_value_txt_label'")
    page(Comum).validar_texto('Renda Mensal')
    page(Comum).validar_texto('PRÓXIMO')
    tap_when_element_exists("* text:'PRÓXIMO'")
    page(Comum).validar_texto('LI E ENTENDI')
    page(Cadastro).click_button("* text:'LI E ENTENDI'", 2)
  end

  def btn_CNH
    page(Comum).contem_texto('Vamos fazer o cadastro do seu documento. Para a foto, precisa ser a CNH ou o RG original.')
    touch("* id:'register_document_selection_card_cnh'")
    page(Comum).validar_texto('ESTOU PRONTO')
    page(Cadastro).click_button("* text:'ESTOU PRONTO'", 2)
  end

  def resp_questoes_doc
    page(Comum).validar_texto('1. A CNH está fora do plástico?')
    page(Comum).validar_elemento('register_document_question_btn_yes')
    page(Cadastro).click_button("* id:'register_document_question_btn_yes'", 2)
    page(Comum).validar_texto('2. A CNH está aberta?')
    page(Comum).validar_elemento('register_document_question_btn_yes')
    page(Cadastro).click_button("* id:'register_document_question_btn_yes'", 2)
    page(Comum).validar_texto('3. A CNH está focada?')
    page(Comum).validar_elemento('register_document_question_btn_yes')
    page(Cadastro).click_button("* id:'register_document_question_btn_yes'", 2)
    page(Comum).validar_texto('4. Todos os campos estão legíveis?')
    page(Comum).validar_elemento('register_document_question_btn_yes')
    page(Cadastro).click_button("* id:'register_document_question_btn_yes'", 2)
  end

  def btn_foto
    sleep 03
    system 'adb shell "input keyevent KEYCODE_CAMERA"'
    sleep 03
    #system 'adb shell "input tap 456 1200"' #motorola
    system 'adb shell "input tap 1200 127"' #samsung S6
  end

  def btn_confirm_image
    page(Comum).validar_texto('posicionar documento', 'Posicione a CNH aberta dentro do retângulo pontilhado')
    page(Comum).validar_elemento('document_image_crop_ctn', 'document_image_crop_view', 'document_image_crop_btn_confirm', 'document_image_crop_btn_rotate','document_image_crop_btn_cancel')
    page(Cadastro).click_button("* id:'document_image_crop_btn_confirm'", 5)
  end

  def ck_endereco
    page(Comum).validar_texto('endereço de entrega', 'Em qual endereço podemos entregar o seu cartão?')
    touch("* id:'delivery_address_radio_current'")
    page(Comum).validar_texto('PRÓXIMO')
    page(Cadastro).click_button("* text:'PRÓXIMO'", 2)
  end

  def edt_assinatura
    touch("* id:'signature_btn_ok'")
    page(Comum).validar_texto('agora é só esperar!')
  end
end
