require "tiny_tds"

class Database 

    def initialize
        @conexao = TinyTds::Client.new(
            :username => MASSA['dba']['username'],
            :password =>  MASSA['dba']['password'],
            :dataserver => MASSA['dba']['server'],
            :database => MASSA['dba']['database']
        )
    end
    
    def executar_query(query)
        @resultado = @conexao.execute("#{query}")
        $resultado = @resultado.each
    end
end


