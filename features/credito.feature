#encoding: utf-8
#language: pt

Funcionalidade: Crédito
  Eu como usuário
  Quero acessar o menu de crédito
  Para conseguir pagar e consultar minhas faturas

@pagamento_fatura
Cenário: Pagar valor total da fatura gerando o boleto
  Dado que acesso o menu de crédito
  Quando realizo o pagamento da fatura com o valor total gerando o boleto
  Então serei encaminhado para browser para baixar o boleto1

Cenário: Pagar valor total da fatura por debito em conta
  Dado que acesso o menu de crédito
  Quando realizo o pagamento da fatura com o valor total por debito em conta
  Então visualizo o copo de credito sem a fatura fechada

@pagamento_adiantado
Cenário: Pagamento adiantado com o valor total gerando o boleto
  Dado que acesso o menu de crédito
  Quando realizo o pagamento adiantado total da fatura com o boleto
  Então serei encaminhado para browser para baixar o boleto2

Cenário: Pagamento adiantado parcial da fatura gerando com boleto
  Dado que acesso o menu de crédito
  Quando realizo o pagamento adiantado parcial da fatura com o boleto
  Então serei encaminhado para browser para baixar o boleto3

Cenário: Pagamento adiantado com o valor total da fatura por debito em conta
  Dado que acesso o menu de crédito
  Quando realizo o pagamento adiantado com o valor total da fatura por debito em conta
  Então visualizo o copo de credito com fatura atual zerada

Cenário: Pagamento adiantado parcial da fatura por debito em conta
  Dado que acesso o menu de crédito
  Quando realizo o pagamento adiantado parcial da fatura por debito em conta
  Então visualizo o copo de credito com o restante da fatura atual

@consulta_faturas
Cenário: Consultar faturas futuras
  Dado que acesso o menu de crédito
  Quando toco em faturas futuras
  Então visualizo os detalhes das faturas futuras

Cenário: Consultar a fatura atual
  Dado que acesso o menu de crédito
  Quando toco em fatura atual
  Então visualizo os detalhes da faturas atual

@consulta_saldo_credor
Cenário: Consultar saldo credor
  Dado que acesso o menu de crédito
  Quando o copo de faturas carregar
  Então visualizo o meu saldo credor
