class TokenCadastro < Calabash::ABase

  include HTTParty

  base_uri "https://cpf-generator.devneon.com.br"
  format :json
  headers 'User-Agent' => 'user_agent'


  def get_token_cadastro(doc)
    sleep 20
    self.class.get("/register/getToken/#{doc}")
  end
end
